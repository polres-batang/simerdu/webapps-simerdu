# Aplikasi Simerdu Polres Batang
## Asset:
- [File Resource](https://drive.google.com/drive/folders/1abbMaSSHe2dKOCF0JDYwJvr22xWleS1m?usp=sharing)
- [Mockup]()

## Backup Database
```sh
docker exec mysql.server /usr/bin/mysqldump -u root --password=root dbweb_simerdu > asset/database.sql
```

## Restore Database
```sh
cat asset/database.sql | docker exec -i mysql.server /usr/bin/mysql -u root --password=root dbweb_simerdu
```

### Email Support
User: support@simerdupolresbatang.id
Pass: MTsgMcFCBO7v

### Privacy Policy
https://www.privacypolicies.com/live/34f04d88-3ecc-41a4-afa6-416803b0388d

### Playstore
App name *
SIMERDU POLRES BATANG

Short description *
Sistem Emergency Terpadu Polres Batang

Full description *
SIMERDU POLRES BATANG

SIMERDU (Sistem Emergency  Terpadu) merupakan platform ketanggapan daruratan satu atap yang berorientasi TEPAT SASARAN dan TEPAT PENANGANAN sebagai jawaban tuntutan masyarakat akan pelayanan prima jika terjadi situasi darurat yang dialami masyarakat.
Berbagai fitur yang telah didesain berbasis high security guna meminimalisir penyalahgunaan platform oleh oknum masyarakat yang sengaja melakukan ghosting call / prank call

Simerdu diharapkan mampu meningkatkan kepuasan masyarakat terhadap layanan ketanggapdaruratan tidak hanya Polres Batang melainkan seluruh stakeholders Kab. Batang

Salam Hormat Kami,
KAPOLRES BATANG
AKBP EDWIN LOUIS SENGKA, S.I.K., M.Si

App icon *
Transparent PNG or JPEG
512 px by 512 px
Up to 1 MB

Feature graphic *
PNG or JPEG
1,024 px by 500 px
Up to 1 MB

Phone screenshots *