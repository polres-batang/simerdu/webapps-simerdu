<?php 
namespace app\backend\controller;
use core\{RestApi, Helper};
use app\backend\service\LaporanService;

class Laporan extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->laporanService = new LaporanService();
        $this->settings = $this->getSettingList();
    }

    public function getAll() {
        Helper::$responseMessage['data'] = $this->laporanService->getAll();
        $this->showResponse(Helper::$responseMessage);
    }

    public function getById($id) {
        Helper::$responseMessage['data'] = $this->laporanService->getById($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function getForm($id) {
        Helper::$responseMessage['data'] = $this->laporanService->getForm($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function search() {
        $fields = $this->getFields(['page' => 1, 'size' => 10, 'cari' => '', 'layanan' => '', 'status' => '']);
        list($page, $size, $cari, $layanan, $status) = array_values($fields);
        $params = [
            'nama_register' => $cari,
            'jenis_layanan' => $layanan,
            'status_laporan' => $status,
        ];

        $data = $this->laporanService->getPage($params, $page, $size);
        $data['query'] = '';
        Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

    public function riwayat($id) {
        $fields = $this->getFields(['page' => 1, 'size' => 10, 'layanan' => '', 'status' => '']);
        list($page, $size, $cari, $layanan, $status) = array_values($fields);
        $params = [
            'id_register' => $id,
            'jenis_layanan' => $layanan,
            'status_laporan' => $status,
        ];

        $data = $this->laporanService->getPage($params, $page, $size);
        $data['contents'] = array_map(function($value) {
            $value['foto_laporan'] = $this->createLink([$value['foto_laporan']]);
            $value['foto_hasil_laporan'] = $this->createLink([$value['foto_hasil_laporan']]);
            return $value;
        }, $data['contents']);

        $data['query'] = '';
        Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

    public function create() {
        $data = $this->getFields();
        $upload = $this->upload('image', [
            'path_upload' => $this->settings['upload']['path_upload_laporan']
        ]);
        $data['foto_laporan'] = $upload['file_upload'] ?: $data['foto_laporan'];
        $upload_result = $this->upload('image_result', [
            'path_upload' => $this->settings['upload']['path_upload_laporan']
        ]);
        $data['foto_hasil_laporan'] = $upload_result['file_upload'] ?: $data['foto_hasil_laporan'];
        $result = $this->laporanService->create($data);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Laporan telah dikirim' : $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function update($id) {
        $data = $this->getFields();
        $upload = $this->upload('image', [
            'path_upload' => $this->settings['upload']['path_upload_laporan']
        ]);
        $data['foto_laporan'] = $upload['file_upload'] ?: $data['foto_laporan'];
        $upload_result = $this->upload('image_result', [
            'path_upload' => $this->settings['upload']['path_upload_laporan']
        ]);
        $data['foto_hasil_laporan'] = $upload_result['file_upload'] ?: $data['foto_hasil_laporan'];
        $result = $this->laporanService->update($id, $data);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Laporan: '.$data['id_laporan'].' telah diubah' : $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function delete($id) {
        $result = $this->laporanService->delete($id);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data laporan telah dihapus' : $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

}

?>