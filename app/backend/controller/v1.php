<?php 
namespace app\backend\controller;
use core\{RestApi, Helper, Fcm};
use core\exception\{NotFoundException};
use oauth\{UserAuth};
use app\backend\service\{
    CallCenterService
};

class v1 extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->auth = UserAuth::getInstance();
        $this->user = new user();
        $this->register = new register();
        $this->monitoring = new monitoring();
        $this->laporan = new laporan();
        $this->broadcast = new broadcast();
        $this->callCenterService = new CallCenterService();
        $this->mobileMapping();
        $this->dashboardMapping();
        $this->monitoringMapping();
        $this->laporanMapping();
        $this->userMapping();
        $this->registerMapping();
        $this->broadcastMapping();
        $this->callcenterMapping();

        $this->requestMapping('GET', '/', function() {
            $this->getRoute();
        });
    }

    /**
     * Mobile Api
     */
    private function mobileMapping() {
        $this->requestMapping('GET', '/mobile/fcm', function() {
            $fcm = new Fcm();
            $topic = $fcm->config['topic'];
            $fcm->setTitle('Info Broadcast');
            $fcm->setTitle('Laporan Arus Mudik 2021');
            $fcm->setImage('https://www.leskompi.com/wp-content/uploads/2018/08/Cara-Mengganti-Background-Foto-Online.png');
            $result = $fcm->sendToTopic($topic, $fcm->getPush());
            print_r($result);

        }, 'Test FCM');

        $this->requestMapping('POST', '/mobile/multipart', function() {
            print_r($_FILES);

        }, 'Test Multipart');

        $this->requestMapping('POST', '/mobile/user/signin', function() {
            $this->auth->mobileValidate();
        }, 'Sign In');

        $this->requestMapping('GET', '/mobile/user/signup/{id}', function($id) {
            $this->register->getForm($id);
        }, 'Form Sign Up');

        $this->requestMapping('POST', '/mobile/user/signup', function() {
            $this->register->create();
        }, 'Sign Up User');

        $this->requestMapping('POST', '/mobile/user/verification/{id}', function($id) {
            $this->register->update($id);
        }, 'Verification User');

        $this->requestMapping('GET', '/mobile/user/reset/{email}', function($email) {
            $this->register->resetPassword($email);
        }, 'Reset Password');

        $this->requestMapping('GET', '/mobile/dashboard', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $callcenter = $this->callCenterService->getForm('CALLCENTER');
            $form_callcenter = $callcenter['form'];
            $form_monitoring = $this->laporan->laporanService->getForm('create');
            $form_action = [];
            foreach ($form_callcenter as $key => $value) {
                $form_action[$key] = $form_monitoring['form'];
                $form_action[$key]['jenis_layanan'] = $key;
                $form_action[$key]['callcenter'] = $value['phone'];
            }

            $web_socket = json_decode(base64_decode($this->settings->socket), true);
            $data['info_room'] = $web_socket['room'];
            $data['info_covid'] = [];
            $data['info_broadcast'] = [];
            $data['form_action'] = $form_action;
            $data['pilihan_kategori'] = $form_monitoring['pilihan_kategori'];
            Helper::$responseMessage['data'] = $data;
            $this->showResponse(Helper::$responseMessage);
        }, 'Mobile Dashboard');

        $this->requestMapping('POST', '/mobile/dashboard/action', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->create();

        }, 'Mobile Dashboard Action');

        $this->requestMapping('POST', '/mobile/broadcast', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->broadcast->search();
        }, 'Show Broadcast List');

        $this->requestMapping('POST', '/mobile/riwayat', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->riwayat($user['id_register']);
        }, 'Show Riwayat laporan');
    }

    /**
     * Admin Dashboard
     */
    private function dashboardMapping() {
        $this->requestMapping('POST', '/dashboard/statistik/{kategori}', function($kategori) {
            list($token, $user) = $this->auth->getAuthorization();
            $params = $this->getFields([
                'page' => 1,
                'size' => 0,
                'start_date' => date('Y-m-d'),
                // 'start_date' => date('Y-m-d', strtotime('-1 months', strtotime('now'))),
                'end_date' => date('Y-m-d'),
            ]);

            switch ($kategori) {
                case 'pengguna':
                    $data = $this->register->registerService->getStatistikPenggunaPerKecamatan($params);
                    break;

                case 'layanan':
                    $data = $this->laporan->laporanService->getStatistikPenggunaanLayanan($params);
                    break;

                case 'pengaduan':
                    $data = $this->laporan->laporanService->getStatistikLayananPengaduan($params);
                    break;
                
                default:
                    throw new NotFoundException('Statistik: '.$kategori.' Not Found');
                    
                    break;
            }

            Helper::$responseMessage['data'] = $data;
            $this->showResponse(Helper::$responseMessage);

        }, 'Statistik laporan');
    }

    /**
     * Admin Monitoring
     */
    private function monitoringMapping() {
        $this->requestMapping('GET', '/monitoring/maps', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->monitoring->getMaps();
        }, 'Get Map Regions');

        $this->requestMapping('GET', '/monitoring/notifikasi', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->monitoring->getNotifikasi();
        }, 'Get Notifikasi');

        $this->requestMapping('GET', '/monitoring/notifikasi/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->monitoring->updateNotifikasi($id);
        }, 'Update & Get Detail Notifikasi');
    }

    /**
     * Admin Laporan
     */
    private function laporanMapping() {
        $this->requestMapping('GET', '/laporan', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->getAll();
        }, 'Get All laporan');

        $this->requestMapping('GET', '/laporan/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->getById($id);
        }, 'Get One laporan');

        $this->requestMapping('GET', '/laporan/form/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->getForm($id);
        }, 'Get Form laporan');

        $this->requestMapping('POST', '/laporan/search', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->search();
        }, 'Search laporan');

        $this->requestMapping('POST', '/laporan', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->create();
        }, 'Create laporan');

        $this->requestMapping('POST', '/laporan/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->update($id);
        }, 'Update laporan');

        $this->requestMapping('DELETE', '/laporan/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->laporan->delete($id);
        }, 'Delete laporan');
    }

    /**
     * Admin User Web
     */
    private function userMapping() {
        $this->requestMapping('GET', '/users', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->user->getAll();
        }, 'Get All User');

        $this->requestMapping('GET', '/users/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->user->getById($id);
        }, 'Get One User');

        $this->requestMapping('GET', '/users/form/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->user->getForm($id);
        }, 'Get Form User');

        $this->requestMapping('POST', '/users/search', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->user->search();
        }, 'Search User');

        // Ada filter username, sehingga proses create dan update terpisah
        $this->requestMapping('POST', '/users', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->user->create();
        }, 'Create User');

        $this->requestMapping('POST', '/users/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->user->update($id);
        }, 'Update User');

        $this->requestMapping('DELETE', '/users/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->user->delete($id);
        }, 'Delete User');
    }

    /**
     * Admin Register User Mobile
     */
    private function registerMapping() {
        $this->requestMapping('GET', '/registers', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->register->getAll();
        }, 'Get All Register');

        $this->requestMapping('GET', '/registers/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->register->getById($id);
        }, 'Get One Register');

        $this->requestMapping('GET', '/registers/form/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->register->getForm($id);
        }, 'Get Form Register');

        $this->requestMapping('POST', '/registers/search', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->register->search();
        }, 'Search Register');

        // Ada filter nomer handphone & identitas, sehingga proses create dan update terpisah
        $this->requestMapping('POST', '/registers', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->register->create();
        }, 'Create Register');

        $this->requestMapping('POST', '/registers/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->register->update($id);
        }, 'Update Register');

        $this->requestMapping('DELETE', '/registers/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->register->delete($id);
        }, 'Delete Register');
    }

    /**
     * * Admin Broadcast Info
     */
    private function broadcastMapping() {
        $this->requestMapping('GET', '/broadcast', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->broadcast->getAll();
        }, 'Get All Broadcast');

        $this->requestMapping('GET', '/broadcast/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->broadcast->getById($id);
        }, 'Get One Broadcast');

        $this->requestMapping('GET', '/broadcast/form/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->broadcast->getForm($id);
        }, 'Get Form Broadcast');

        $this->requestMapping('POST', '/broadcast/search', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->broadcast->search();
        }, 'Search Broadcast');

        $this->requestMapping('POST', '/broadcast', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $this->broadcast->create();
        }, 'Create Broadcast');

        $this->requestMapping('DELETE', '/broadcast/{id}', function($id) {
            list($token, $user) = $this->auth->getAuthorization();
            $this->broadcast->delete($id);
        }, 'Delete Broadcast');
    }

    /**
     * Admin Call Center
     */
    private function callcenterMapping() {
        $this->requestMapping('GET', '/callcenter/form', function() {
            list($token, $user) = $this->auth->getAuthorization();
            Helper::$responseMessage['data'] = $this->callCenterService->getForm('CALLCENTER');
            $this->showResponse(Helper::$responseMessage);
        }, 'Get Form Call Center');

        $this->requestMapping('POST', '/callcenter', function() {
            list($token, $user) = $this->auth->getAuthorization();
            $form = $this->callCenterService->getForm('CALLCENTER');
            $data = $this->getFields($form['model']);
            $result = $this->callCenterService->create($data);
            Helper::$responseMessage['message'] = !is_null($result) ? 'Perubahan data telah disimpan' : 'Perubahan data gagal disimpan';
            Helper::$responseMessage['data'] = $result;
            $this->showResponse(Helper::$responseMessage);
        }, 'Create/Update Call Center');
    }

}

?>