<?php 
namespace app\backend\controller;
use core\{RestApi, Helper};
use app\backend\service\UserService;

class User extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->userService = new UserService();
    }

    public function getAll() {
        Helper::$responseMessage['data'] = $this->userService->getAll();
        $this->showResponse(Helper::$responseMessage);
    }

    public function getById($id) {
        Helper::$responseMessage['data'] = $this->userService->getById($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function getForm($id) {
        Helper::$responseMessage['data'] = $this->userService->getForm($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function search() {
        $fields = $this->getFields(['page' => 1, 'size' => 10, 'cari' => '']);
        list($page, $size, $cari) = array_values($fields);
        $params = [
            'username' => $cari,
        ];

        $data = $this->userService->getPage($params, $page, $size);
        $data['query'] = '';
        Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

    public function create() {
        $data = $this->getBody();
        $result = $this->userService->create($data);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data user: '.$data['username'].' telah disimpan' : 'Data user: '.$data['username'].' gagal disimpan';
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function update($id) {
        $data = $this->getBody();
        $result = $this->userService->update($id, $data);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data user: '.$data['username'].' telah diubah' : 'Data user: '.$data['username'].' gagal diubah';
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function delete($id) {
        $result = $this->userService->delete($id);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data user: '.$result['data']['username'].' telah dihapus' : 'Data gagal dihapus';
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

}

?>