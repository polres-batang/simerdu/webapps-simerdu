<?php 
namespace app\backend\controller;
use core\{RestApi, Helper, Fcm};
use app\backend\service\BroadcastService;

class Broadcast extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->broadcastService = new BroadcastService();
        $this->settings = $this->getSettingList();
    }

    public function getAll() {
        Helper::$responseMessage['data'] = $this->broadcastService->getAll();
        $this->showResponse(Helper::$responseMessage);
    }

    public function getById($id) {
        Helper::$responseMessage['data'] = $this->broadcastService->getById($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function getForm($id) {
        Helper::$responseMessage['data'] = $this->broadcastService->getForm($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function search() {
        $fields = $this->getFields(['page' => 1, 'size' => 10, 'cari' => '']);
        list($page, $size, $cari) = array_values($fields);
        $params = [
            'judul_broadcast' => $cari,
            'sumber_broadcast' => $cari,
        ];

        $data = $this->broadcastService->getPage($params, $page, $size);
        $data['contents'] = array_map(function($value) {
            $value['gambar_broadcast'] = $this->createLink([$value['gambar_broadcast']]);
            return $value;
        }, $data['contents']);

        $data['query'] = '';
        Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

    public function create() {
        $data = $this->getFields();
        $upload = $this->upload('image', [
            'path_upload' => $this->settings['upload']['path_upload_broadcast']
        ]);
        $data['gambar_broadcast'] = $upload['file_upload'] ?: $data['gambar_broadcast'];
        $result = $this->broadcastService->create($data);
        if ($result['success']) {
            $fcm = new Fcm();
            $topic = $fcm->config['topic'];
            $image = $this->createLink([$this->settings['upload']['path_upload_broadcast'], $data['gambar_broadcast']]);
            // $image = 'https://www.leskompi.com/wp-content/uploads/2018/08/Cara-Mengganti-Background-Foto-Online.png';
            $fcm->setTitle('Info Broadcast');
            $fcm->setMessage($data['judul_broadcast']);
            $fcm->setPayload($result['data']);
            $fcm->setImage($image);
            $fcm->sendToTopic($topic, $fcm->getPush());
        }
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data telah disimpan' : $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function delete($id) {
        $result = $this->broadcastService->delete($id);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data telah dihapus' : $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

}

?>