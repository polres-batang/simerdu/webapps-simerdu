<?php 
namespace app\backend\controller;
use core\{RestApi, Helper};
use app\backend\service\{
    RegionsService,
    LaporanService
};

class Monitoring extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->regionsService = new RegionsService();
        $this->laporanService = new LaporanService();
    }

    public function getMaps() {
        Helper::$responseMessage['data'] = $this->regionsService->getRegions();
        $this->showResponse(Helper::$responseMessage);
    }

    public function getNotifikasi() {
        $data = $this->laporanService->getNotifikasi();
        unset($data['query']);
        Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

    public function updateNotifikasi($id) {
        $data = $this->laporanService->update($id, ['status_laporan' => "process"]);
        $data = $this->laporanService->getDetailLaporan($id);
        Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

}

?>