<?php 
namespace app\backend\controller;
use core\{RestApi, Helper};
use core\exception\ConnectionException;
use app\backend\service\RegisterService;
use PHPMailer\PHPMailer\PHPMailer;

class Register extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->registerService = new RegisterService();
    }

    public function getAll() {
        Helper::$responseMessage['data'] = $this->registerService->getAll();
        $this->showResponse(Helper::$responseMessage);
    }

    public function getById($id) {
        Helper::$responseMessage['data'] = $this->registerService->getById($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function getForm($id) {
        Helper::$responseMessage['data'] = $this->registerService->getForm($id);
        $this->showResponse(Helper::$responseMessage);
    }

    public function search() {
        $fields = $this->getFields(['page' => 1, 'size' => 10, 'cari' => '']);
        list($page, $size, $cari) = array_values($fields);
        $params = [
            'nomer_handphone' => $cari,
            'nama_register' => $cari,
        ];

        $data = $this->registerService->getPage($params, $page, $size);
        $data['query'] = '';
        Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

    public function create() {
        $data = $this->getBody();
        $result = $this->registerService->create($data);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data user: '.$data['nama_register'].' telah disimpan' : 'Data user: '.$data['nama_register'].' gagal disimpan';
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function update($id) {
        $data = $this->getBody();
        $result = $this->registerService->update($id, $data);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data user: '.$data['nama_register'].' telah diubah' : 'Data user: '.$data['nama_register'].' gagal diubah';
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function delete($id) {
        $result = $this->registerService->delete($id);
        Helper::$responseMessage['message'] = ($result['success']) ? 'Data user: '.$result['data']['nama_register'].' telah dihapus' : 'Data gagal dihapus';
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function resetPassword($email) {
        // sementara dapatkan password yang aktif, belum implement password baru
        $data = $this->registerService->getByEmail($email);
        $data = $this->registerService->getById($data['id_register']); // Dapatkan plain password
        $this->sendEmail([
            'user_name' => $data['nama_register'],
            'email_address' => $data['email_register'],
            'email_subject' => 'Reset Password Account Simerdu',
            'email_body' => 'Password account Anda adalah :'. $data['password'],
        ]);

        Helper::$responseMessage['message'] = 'Password baru telah dikirim ke email '.$email;
        // Helper::$responseMessage['data'] = $data;
        $this->showResponse(Helper::$responseMessage);
    }

    /**
     * Setting konfigurasi email account
     * Cpanel -> Email -> Akun Email
     * Klik buat, isi form pembuatan email account
     * Pilih akun email yang baru saja dibuat, lalu klik manage
     * Klik menu Connect Devices
     * Pilih konfigurasi yang disarankan (SSL/TLS)
     * 
     * Setting Sender Policy Framework (SPF), digunakan untuk mengurangi indikasi spam pada email server tujuan
     * Cpanel -> Email -> Email Deliverability
     * Pilih Nama Domain yang akan mengaktifkan SPF, klik Manage
     * Scroll Ke bawah, trus cari dan klik tombol customize
     * Setelah itu klik Install a customized SPF record
     * Tunggu 24jam, untuk update DNS Record
     * Check konfigurasi di Cpanel -> Domain -> Zone Editor
     * Klik Kelola (Manage), cari kata spf
     */
    private function sendEmail($data) {
        set_time_limit(0);
        $mail = new PHPMailer;
        $mail->isSMTP();
        // $mail->SMTPDebug = 2;
        $mail->Host = 'ssl://mail.simerdupolresbatang.id';
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for GMail
        $mail->Username = 'support@simerdupolresbatang.id'; // email account in hosting
        $mail->Password = 'MTsgMcFCBO7v'; // password email account
        $mail->setFrom('support@simerdupolresbatang.id', 'Admin Simerdu');
        $mail->addReplyTo('support@simerdupolresbatang.id', 'Admin Simerdu');
        $mail->addAddress($data['email_address'], $data['user_name']);
        $mail->Subject = $data['email_subject'];
        // $mail->msgHTML(file_get_contents('message.html'), __DIR__);
        // $mail->Body = 'This is a plain text message body';
        $mail->Body = $data['email_body'];
        //$mail->addAttachment('test.txt');
        if (!$mail->send()) {
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
            throw new ConnectionException($mail->ErrorInfo);
        }
    }

}

?>