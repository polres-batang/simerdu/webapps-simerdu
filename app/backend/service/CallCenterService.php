<?php 
namespace app\backend\service;
use core\exception\{NotFoundException, ConnectionException, ValidationException};
use core\{Helper};
use app\backend\repository\{CallCenter};

class CallCenterService {

    public function __construct() {
        $this->callcenter = new CallCenter();
    }

    public function getForm($id) {
        $data = $this->callcenter->getById($id);
        $model = $data ?? $this->callcenter->getModel();
        $form = [];
        foreach ($model as $key => $value) {
            if ($key != 'id_callcenter') {
                $form[$key] = [
                    'icon' => '/asset/image/ic_'.$key.'.png',
                    'text' => strtoupper($key),
                    'phone' => $value,
                ];
            }
        }

        $result['title'] = 'Call Center';
        $result['model'] = $model;
        $result['form'] = $form;
        return $result;
    }

    public function create($data) {
        return $this->callcenter->save($data);
    }

}

?>