<?php 
namespace app\backend\service;
use core\exception\{NotFoundException, ConnectionException, ValidationException};
use core\{Helper};
use app\backend\repository\{Register};

class RegisterService {

    public function __construct() {
        $this->register = new Register();
    }

    public function getAll() {
        return $this->register->getAll();
    }

    public function getPage($params, $page, $size) {
        list($conditions, $params) = $this->register->createQueryParams($params, 'LIKE');
        $where = ($params) ? ' WHERE '.implode(' OR ', $conditions) : '';
        $query = [
            'value' => 'SELECT * FROM '.$this->register->getTable().$where,
            'total' => 'SELECT COUNT(*) AS total FROM '.$this->register->getTable().$where
        ];
        $result = $this->register->getPage($query, $params, $page, $size);
        return $result;
    }

    public function getById($id) {
        $data = $this->register->getById($id);
        if (is_null($data)) {
            throw new NotFoundException('Register ID: '.$id.' Not Found');
        }

        return $data;
    }

    public function getByEmail($email) {
        $data = $this->register->getByFields(['email_register' => $email]);
        if (is_null($data)) {
            throw new NotFoundException('Register with email: '.$email.' Not Found');
        }

        return $data;
    }

    public function getForm($id) {
        $result['title'] = 'Edit Data Register';
        $result['form'] = $this->register->getById($id);
        $result['pilihan_kecamatan'] = $this->getPilihanKecamatan();
        $result['pilihan_keldesa'] = $this->getPilihanKeldesa();
        if (is_null($result['form'])) {
            $result['title'] = 'Input Data Register';
            $result['form'] = $this->register->getModel();
        }

        return $result;
    }

    public function create($data) {
        $info = [
            'nomer_handphone' => 'Nomer Handphone',
            'nomer_identitas' => 'Nomer Indentitas',
        ];
        $params = [
            'nomer_handphone' => $data['nomer_handphone'],
            'nomer_identitas' => $data['nomer_identitas'],
        ];
        if(is_null($this->register->getByFields($params, 'OR'))) {
            return $this->register->save($data);
        }
        else {
            $message = [];
            foreach ($params as $key => $value) {
                $message[] = $info[$key].': '.$value;
            }
            throw new ValidationException(implode(' atau ', $message).' sudah pernah terdaftar !!');
        }
    }

    public function update($id, $update) {
        $data = $this->getById($id);
        $data = Helper::filterParams($data, $update);
        return $this->register->save($data);
    }

    public function delete($id) {
        $data = $this->getById($id);
        return $this->register->delete($data);
    }

    /**
     * Custom Service
     */
    public function getPilihanKecamatan($id = '3325%') {
        $result = ['' => ['text' => '-- Pilih Kecamatan --']];
        $query = 'SELECT * FROM tb_kecamatan WHERE kabkota_id LIKE ?';
        $data = $this->register->getQuery($query, [$id]);
        foreach ($data['value'] as $key => $value) {
            $result[$value['id_kecamatan']] = [
                'text' => $value['nama_kecamatan'],
            ];
        }

        return $result;
    }

    public function getPilihanKeldesa($id = '3325%') {
        $result = ['' => ['text' => '-- Pilih Kecamatan --']];
        $query = 'SELECT * FROM tb_keldesa WHERE kecamatan_id LIKE ?';
        $data = $this->register->getQuery($query, [$id]);
        foreach ($data['value'] as $key => $value) {
            $result[$value['id_keldesa']] = [
                'text' => $value['nama_keldesa'],
                'group' => $value['kecamatan_id'],
            ];
        }

        return $result;
    }

    public function getStatistikPenggunaPerKecamatan($params) {
        $page = $params['page'];
        $size = $params['size'];
		$start_date = $params['start_date'];
		$end_date = $params['end_date'];
        $cursor = ($page - 1) * $size;
		
        $query = 'SELECT 
                kecamatan.*,
                -- ROUND(RAND() * 100) AS jumlah_pengguna
                COUNT(register.`id_register`) AS jumlah_pengguna
                FROM `tb_kecamatan` kecamatan
                JOIN `tb_keldesa` keldesa ON (keldesa.`kecamatan_id`=kecamatan.`id_kecamatan`)
                LEFT JOIN `tb_register` register ON (register.`keldesa_id`=keldesa.`id_keldesa`)
                WHERE (kecamatan.`kabkota_id` = "3325")
                GROUP BY kecamatan.`id_kecamatan`';
        $query .= ' ORDER BY jumlah_pengguna DESC';

        if ($size == 0) {
            $result = $this->register->getQuery($query);
		}
		else {
            $result = $this->register->getQuery($query.' LIMIT '.$cursor.','.$size);
		}
        
        $result = Helper::formatCharts($result['value'], 'nama_kecamatan', 'jumlah_pengguna');
        return $result;
    }

}

?>