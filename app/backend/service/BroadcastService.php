<?php 
namespace app\backend\service;
use core\exception\{NotFoundException, ConnectionException, ValidationException};
use core\{Helper};
use app\backend\repository\{Broadcast};

class BroadcastService {

    public function __construct() {
        $this->broadcast = new Broadcast();
        $this->main_setting = $this->broadcast->getSettings();
        $this->default_image = '/upload/default/no-user-image.png';
    }

    public function getAll() {
        return $this->broadcast->getAll();
    }

    public function getPage($params, $page, $size) {
        list($conditions, $params) = $this->broadcast->createQueryParams($params, 'LIKE');
        $where = ($params) ? ' WHERE '.implode(' OR ', $conditions) : '';
        $query = 'SELECT {fields} FROM '.$this->broadcast->getTable();
        $query .= $where.' ORDER BY datetime DESC';
        $query = [
            'value' => str_replace('{fields}', '*', $query),
            'total' => str_replace('{fields}', 'COUNT(*) AS total', $query)
        ];
        $result = $this->broadcast->getPage($query, $params, $page, $size);
        foreach ($result['contents'] as $key => $value) {
            $result['contents'][$key]['short_broadcast'] = substr($value['isi_broadcast'], 0, 100).'...';
            $result['contents'][$key]['tanggal_broadcast'] = Helper::dateFormat($value['datetime'], 'long_date_time');
            $result['contents'][$key]['gambar_broadcast'] = empty($value['gambar_broadcast']) ? $this->default_image : $this->main_setting['upload']['path_upload_broadcast'].$value['gambar_broadcast'];
        }
        return $result;
    }

    public function getById($id) {
        $data = $this->broadcast->getById($id);
        if (is_null($data)) {
            throw new NotFoundException('Broadcast ID: '.$id.' Not Found');
        }

        return $data;
    }

    public function getForm($id) {
        $result['title'] = 'Edit Broadcast';
        $result['form'] = $this->broadcast->getById($id);
        if (is_null($result['form'])) {
            $result['title'] = 'Input Broadcast';
            $result['form'] = $this->broadcast->getModel();
        }

        return $result;
    }

    public function create($data) {
        return $this->broadcast->save($data);
    }

    public function update($id, $update) {
        $data = $this->getById($id);
        $data = Helper::filterParams($data, $update);
        return $this->broadcast->save($data);
    }

    public function delete($id) {
        $data = $this->getById($id);
        return $this->broadcast->delete($data);
    }

}

?>