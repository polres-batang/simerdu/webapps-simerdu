<?php 
namespace app\backend\service;
use core\exception\{NotFoundException, ConnectionException, ValidationException};
use core\{Helper};
use oauth\{UserAuth};
use app\backend\repository\{Laporan};

class LaporanService {
    /**
     * Flow Laporan:
     * Action:
     * - Kecelakaan
     * - Kemacetan
     * - Kebakaran
     * - Kejahatan
     * - Bencana
     * - Bengkel
     * - Donor
     * -> Foto laporan user -> call admin -> dashboard monitoring -> share laporan ke petugas via whatsapp -> entry/upload foto hasil laporan -> selesai
     * 
     * - Covid19
     * - Pengaduan
     * -> Pilih Kategori -> Foto laporan user -> call admin/direct message -> dashboard monitoring -> share laporan ke petugas via whatsapp -> entry/upload foto hasil laporan -> selesai
     */
    public function __construct() {
        $this->auth = UserAuth::getInstance();
        $this->session = $this->auth->getSession();
        $this->laporan = new Laporan();
        $this->registerService = new RegisterService();
        $this->callCenterService = new CallCenterService();
        $this->main_setting = $this->laporan->getSettings();
        $this->default_image = '/upload/default/no-user-image.png';
    }

    public function getAll() {
        return $this->laporan->getAll();
    }

    public function getPage($params, $page, $size) {
        list($conditions, $params) = $this->laporan->createQueryParams($params, 'LIKE');
        $where = ($params) ? ' WHERE '.implode(' AND ', $conditions) : '';
        $query = 'SELECT {fields} 
                FROM `tb_laporan` laporan 
                JOIN `tb_register` register ON (laporan.register_id=register.id_register)
                JOIN `tb_kecamatan` kecamatan ON (register.kecamatan_id=kecamatan.id_kecamatan)
                JOIN `tb_keldesa` keldesa ON (register.keldesa_id=keldesa.id_keldesa)';
        
        // check user level (admin|propam|user)
        $level = $this->session->data->level;
        switch ($level) {
            case 'admin':
                $query .= $where.' AND jenis_layanan <> "pengaduan"';
                break;

            case 'propam':
                $query .= $where.' AND jenis_layanan = "pengaduan"';
                break;
            
            default:
                $query .= $where;
                break;
        }

        $query .= ' ORDER BY lastupdate DESC';
        $query = [
            'value' => str_replace('{fields}', 'laporan.*, register.id_register, register.nomer_handphone, register.nomer_identitas, register.nama_register, register.alamat_register, register.email_register, kecamatan.nama_kecamatan, keldesa.nama_keldesa', $query),
            'total' => str_replace('{fields}', 'COUNT(*) AS total', $query)
        ];
        $result = $this->laporan->getPage($query, $params, $page, $size);
        $result['level'] = $user['level'];
        foreach ($result['contents'] as $key => $value) {
            $result['contents'][$key] = $this->formatContents($value);
        }
        return $result;
    }

    public function getById($id) {
        $data = $this->laporan->getById($id);
        if (is_null($data)) {
            throw new NotFoundException('Laporan ID: '.$id.' Not Found');
        }

        return $data;
    }

    public function getForm($id) {
        $result['title'] = 'Edit Data Laporan';
        $result['form'] = $this->laporan->getById($id);
        $result['pilihan_user'] = $this->getPilihanUser();
        $result['pilihan_layanan'] = $this->getPilihanLayanan();
        $result['pilihan_kategori'] = $this->getKategoriLaporan();
        $result['pilihan_media'] = $this->getMediaLaporan();
        $result['pilihan_status'] = $this->getStatusLaporan();
        if (is_null($result['form'])) {
            $result['title'] = 'Input Data Laporan';
            $result['form'] = $this->laporan->getModel();
        }

        return $result;
    }

    public function getNotifikasi() {
        $params = [
            'status_laporan' => 'pending'
        ];
        list($conditions, $params) = $this->laporan->createQueryParams($params, 'LIKE');
        $where = ($params) ? ' WHERE '.implode(' AND ', $conditions) : '';
        $query = 'SELECT laporan.*,
                register.nomer_handphone, register.nomer_identitas, register.nama_register, register.alamat_register, register.email_register,
                kecamatan.nama_kecamatan,
                keldesa.nama_keldesa
                FROM `tb_laporan` laporan 
                JOIN`tb_register` register ON (laporan.register_id=register.id_register)
                JOIN `tb_kecamatan` kecamatan ON (register.kecamatan_id=kecamatan.id_kecamatan)
                JOIN `tb_keldesa` keldesa ON (register.keldesa_id=keldesa.id_keldesa)';

        // check user level (admin|propam|user)
        $level = $this->session->data->level;
        switch ($level) {
            case 'admin':
                $query .= $where.' AND jenis_layanan <> "pengaduan"';
                break;

            case 'propam':
                $query .= $where.' AND jenis_layanan = "pengaduan"';
                break;
            
            default:
                $query .= $where;
                break;
        }

        $query .= ' ORDER BY lastupdate DESC';
        $result = $this->laporan->getQuery($query, $params);
        foreach ($result['value'] as $key => $value) {
            $result['value'][$key] = $this->formatContents($value);
        }
        return $result;
    }

    public function create($data) {
        return $this->laporan->save($data);
    }

    public function update($id, $update) {
        $data = $this->getById($id);
        $data = Helper::filterParams($data, $update);
        return $this->laporan->save($data);
    }

    public function delete($id) {
        $data = $this->getById($id);
        return $this->laporan->delete($data);
    }

    public function getDetailLaporan($id) {
        list($conditions, $params) = $this->laporan->createQueryParams(['id_laporan' => $id], '=');
        $where = ($params) ? ' WHERE '.implode(' AND ', $conditions) : '';
        $query = 'SELECT laporan.*,
                register.nomer_handphone, register.nomer_identitas, register.nama_register, register.alamat_register, register.email_register,
                kecamatan.nama_kecamatan,
                keldesa.nama_keldesa
                FROM `tb_laporan` laporan 
                JOIN`tb_register` register ON (laporan.register_id=register.id_register)
                JOIN `tb_kecamatan` kecamatan ON (register.kecamatan_id=kecamatan.id_kecamatan)
                JOIN `tb_keldesa` keldesa ON (register.keldesa_id=keldesa.id_keldesa)';
        $query .= $where.' ORDER BY lastupdate DESC';
        $result = $this->laporan->getQuery($query, $params);
        $data = ($result['count'] > 0) ? current($result['value']) : null;
        if (is_null($data)) {
            throw new NotFoundException('Laporan ID: '.$id.' Not Found');
        }

        $result = $this->formatContents($data);
        return $result;
    }

    /**
     * Custom Service
     */
    public function getPilihanUser() {
        $result = ['' => ['text' => '-- Pilih User --']];
        $register = $this->registerService->getALL();
        foreach ($register as $key => $value) {
            $result[$value['id_register']] = [
                'text' => $value['nama_register'],
            ];
        }

        return $result;
    }

    public function getPilihanLayanan() {
        $result = ['' => ['text' => '-- Pilih Layanan --']];
        $callcenter = $this->callCenterService->getForm('CALLCENTER');
        $result += $callcenter['form'];
        
        // check user level (admin|propam|user)
        $level = $this->session->data->level;
        switch ($level) {
            case 'admin':
                unset($result['pengaduan']);
                break;

            case 'propam':
                $result = array_filter($result, function($key) {
                    return in_array($key, ['', 'pengaduan']);
                }, ARRAY_FILTER_USE_KEY);
                break;
            
            default:
                
                break;
        }

        return $result;
    }

    public function getKategoriLaporan() {
        return [
            'covid19' => [
                'C01' => ['text' => 'Kawasan/Tempat tidak tertib protokol kesehatan'],
                'C02' => ['text' => 'Kerumunan tidak tertib protokol kesehatan'],
                'C03' => ['text' => 'Laporan indikasi positif Covid-19'],
                'Z99' => ['text' => 'Lainnya...'],
            ],
            'pengaduan' => [
                'P01' => ['text' => 'Ketidakpuasan pelayanan SIM, SKCK, BPKB, STNK, dll'],
                'P02' => ['text' => 'Ketidakpuasan proses penyelidikan'],
                'P03' => ['text' => 'Kekerasan Oknum'],
                'P04' => ['text' => 'Penyalahgunaan Kewenangan'],
                'P05' => ['text' => 'Pungli'],
                'Z99' => ['text' => 'Lainnya...'],
            ]
        ];
    }

    public function getStatusLaporan() {
        return [
            'pending' => ['text' => 'Menunggu', 'color' => 'warning'],
            'process' => ['text' => 'Diproses', 'color' => 'info'],
            'complete' => ['text' => 'Selesai', 'color' => 'success'],
        ];
    }

    public function getMediaLaporan() {
        return [
            'call' => ['text' => 'By Phone'],
            'sms' => ['text' => 'By Message'],
        ];
    }

    public function getPilihanSizeLimit(){
		return [
			'0' => ['text' => 'Semua Data'],
			'15' => ['text' => '15 Data'],
			'10' => ['text' => '10 Data'],
			'5' => ['text' => '5 Data'],
		];
	}

    /**
     * Statistik Dashboard Admin
     */
    public function getStatistikPenggunaanLayanan($params) {
        $page = $params['page'];
        $size = $params['size'];
		$start_date = $params['start_date'];
		$end_date = $params['end_date'];
        // $cursor = ($page - 1) * $size;

        $callcenter = $this->callCenterService->getForm('CALLCENTER');
        $layanan = $callcenter['model'];
        unset($layanan['id_callcenter']);
        $layanan = array_keys($layanan);
        $result = [];

        foreach ($layanan as $key => $value) {
            $where = 'WHERE (laporan.`jenis_layanan` = "'.$value.'") AND (date(laporan.`tanggal_laporan`) BETWEEN "'.$start_date.'" AND "'.$end_date.'")';
            $query = 'SELECT IF(COUNT(*) > 0, COUNT(*), ROUND(RAND() * 100)) AS jumlah_laporan FROM `tb_laporan` laporan '.$where;
            $query = 'SELECT COUNT(*) AS jumlah_laporan FROM `tb_laporan` laporan '.$where;
            $data_layanan = $this->laporan->getQuery($query);
            array_push($result, [
                'nama_layanan' => strtoupper($value),
                'jumlah_laporan' => $data_layanan['value'][0]['jumlah_laporan'],
                'query' => $data_layanan['query'],
            ]);
        }

        $result = Helper::formatCharts($result, 'nama_layanan', 'jumlah_laporan');
        return $result;
    }

    public function getStatistikLayananPengaduan($params) {
        $page = $params['page'];
        $size = $params['size'];
		$start_date = $params['start_date'];
		$end_date = $params['end_date'];
        // $cursor = ($page - 1) * $size;

        $kategori = $this->getKategoriLaporan();
        $layanan = $kategori['pengaduan'];
        $result = [];

        foreach ($layanan as $key => $value) {
            $where = 'WHERE (laporan.`jenis_layanan` = "pengaduan") AND (laporan.`kategori_layanan` = "'.$key.'") AND (date(laporan.`tanggal_laporan`) BETWEEN "'.$start_date.'" AND "'.$end_date.'")';
            $query = 'SELECT IF(COUNT(*) > 0, COUNT(*), ROUND(RAND() * 100)) AS jumlah_laporan FROM `tb_laporan` laporan '.$where;
            $query = 'SELECT COUNT(*) AS jumlah_laporan FROM `tb_laporan` laporan '.$where;
            $data_layanan = $this->laporan->getQuery($query);
            array_push($result, [
                'nama_layanan' => $value['text'],
                'jumlah_laporan' => $data_layanan['value'][0]['jumlah_laporan'],
                'query' => $data_layanan['query'],
            ]);
        }

        $result = Helper::formatCharts($result, 'nama_layanan', 'jumlah_laporan');
        return $result;
    }

    public function formatContents($contents) {
        $result = $contents;
        $pilihan_layanan = $this->getPilihanLayanan();
        $pilihan_kategori = $this->getKategoriLaporan();
        $pilihan_status = $this->getStatusLaporan();
        $pilihan_media = $this->getMediaLaporan();

        $nama_layanan = $pilihan_layanan[$contents['jenis_layanan']]['text'];
        $nama_pelapor = $contents['nama_register'];
        $nomer_handphone = $contents['nomer_handphone'];
        $latitude = $contents['latitude'];
        $longitude = $contents['longitude'];
        $lastupdate = Helper::dateFormat($contents['lastupdate'], 'long_date_time');

        $message = <<<EOF
        *SIMERDU REPORT*
        Tanggal $lastupdate
        *$nama_layanan*

        PELAPOR: $nama_pelapor
        HP: $nomer_handphone
        LOCATION:
        https://www.google.com/maps/search/?api=1&query=$latitude,$longitude
        EOF;

        // $contents['kategori_layanan'] = '';
        $result['nama_layanan'] = $nama_layanan;
        $result['nama_kategori'] = $pilihan_kategori[$contents['jenis_layanan']][$contents['kategori_layanan']]['text'] ?: '...';
        $result['tanggal_laporan'] = Helper::dateFormat($contents['tanggal_laporan'], 'long_date');
        $result['status_laporan_text'] = $pilihan_status[$contents['status_laporan']]['text'];
        $result['status_laporan_color'] = $pilihan_status[$contents['status_laporan']]['color'];
        $result['media_laporan_text'] = $pilihan_media[$contents['media_laporan']]['text'];
        $result['nama_pelapor'] = $nama_pelapor;
        $result['alamat_pelapor'] = $contents['alamat_register'];
        $result['foto_laporan'] = empty($contents['foto_laporan']) ? $this->default_image : $this->main_setting['upload']['path_upload_laporan'].$contents['foto_laporan'];
        $result['foto_hasil_laporan'] = empty($contents['foto_hasil_laporan']) ? $this->default_image : $this->main_setting['upload']['path_upload_laporan'].$contents['foto_hasil_laporan'];
        $result['send_whatsapp'] = rawurlencode($message);

        return $result;
    }

}

?>