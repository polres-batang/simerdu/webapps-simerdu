<?php 
namespace app\backend\service;
use core\exception\{NotFoundException, ConnectionException, ValidationException};
use core\{Helper};
use app\backend\repository\{Regions};

class RegionsService {

    public function __construct() {
        $this->regions = new Regions();
    }

    public function getRegions() {
        $data = $this->regions->getById('batangkab');
        if (is_null($data)) {
            throw new NotFoundException('Regions ID: '.$id.' Not Found');
        }

        return $data;
    }

}

?>