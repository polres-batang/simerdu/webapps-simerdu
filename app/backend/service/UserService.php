<?php 
namespace app\backend\service;
use core\exception\{NotFoundException, ConnectionException, ValidationException};
use core\{Helper};
use app\backend\repository\{User};

class UserService {

    public function __construct() {
        $this->user = new User();
    }

    public function getAll() {
        return $this->user->getAll();
    }

    public function getPage($params, $page, $size) {
        $result = $this->user->getPage('OR', $params, $page, $size);
        return $result;
    }

    public function getById($id) {
        $data = $this->user->getById($id);
        if (is_null($data)) {
            throw new NotFoundException('User ID: '.$id.' Not Found');
        }

        return $data;
    }

    public function getForm($id) {
        $result['title'] = 'Edit Data User';
        $result['form'] = $this->user->getById($id);
        $result['pilihan_level'] = $this->getPilihanLevel();
        if (is_null($result['form'])) {
            $result['title'] = 'Input Data Register';
            $result['form'] = $this->user->getModel();
        }

        return $result;
    }

    public function create($data) {
        $info = [
            'username' => 'Username',
        ];
        $params = [
            'username' => $data['username'],
        ];
        if(is_null($this->user->getByFields($params, 'OR'))) {
            return $this->user->save($data);
        }
        else {
            $message = [];
            foreach ($params as $key => $value) {
                $message[] = $info[$key].': '.$value;
            }
            throw new ValidationException(implode(' atau ', $message).' sudah pernah terdaftar !!');
        }
    }

    public function update($id, $update) {
        $data = $this->getById($id);
        $data = Helper::filterParams($data, $update);
        return $this->user->save($data);
    }

    public function delete($id) {
        $data = $this->getById($id);
        return $this->user->delete($data);
    }

    /**
     * Custom Service
     */
    public function getPilihanLevel() {
        return [
            '' => ['text' => '-- Pilih Level --'],
            'admin' => ['text' => 'Administrator'],
            'propam' => ['text' => 'Bid. Propam'],
        ];
    }

}

?>