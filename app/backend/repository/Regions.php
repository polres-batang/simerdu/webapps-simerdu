<?php 
namespace app\backend\repository;
use core\database\Json;

class Regions extends Json {

    public function __construct() {
        parent::__construct('region', 'tb_regions', 'id_regions');
        parent::setModel([
            'id_regions' => 'batangkab',
            'latitude' => '',
            'longitude' => '',
            'borderline' => '',
        ]);
    }

}
?>