<?php 
namespace app\backend\repository;
use core\database\Mysql;

class Laporan extends Mysql {

    public function __construct() {
        parent::__construct('mysqldb', 'tb_laporan', 'id_laporan');
        parent::setModel([
            'id_laporan' => $this->setRandomID(),
            'register_id' => '',
            'tanggal_laporan' => date('Y-m-d'),
            'jenis_layanan' => '',
            'kategori_layanan' => '',
            'kategori_lainnya' => '', // Entry isian jika pilihan kategori lainnya
            'media_laporan' => '', // call admin | direct message
            'foto_laporan' => '', // foto kiriman dari user
            'foto_hasil_laporan' => '', // foto hasil tindakan dari petugas
            'keterangan_hasil_laporan' => '', // keterangan hasil tindakan dari petugas
            'keterangan_tindak_lanjuti' => '', // keterangan saat petugas, melakukan tindak lanjuti (optional)
            'status_laporan' => 'pending', //pending|process|complete
            'latitude' => '0',
            'longitude' => '0',
            'lastupdate' => date('Y-m-d H:i:s'),
        ]);
    }

}
?>