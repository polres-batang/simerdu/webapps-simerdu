<?php 
namespace app\backend\repository;
use core\database\Mysql;

class Broadcast extends Mysql {

    public function __construct() {
        parent::__construct('mysqldb', 'tb_broadcast', 'id_broadcast');
        parent::setModel([
            'id_broadcast' => $this->setRandomID(),
            'judul_broadcast' => '',
            'isi_broadcast' => '',
            'gambar_broadcast' => '',
            'sumber_broadcast' => '',
            'datetime' => date('Y-m-d H:i:s'),
        ]);
    }

}
?>