<?php 
namespace app\backend\repository;
use core\database\Json;

class User extends Json {

    public function __construct() {
        parent::__construct('user', 'tb_user', 'id_user');
        parent::setModel([
            'id_user' => $this->setRandomID(),
            'username' => '',
            'password' => '',
            'fullname' => 'Administrator',
            'level' => 'admin', // admin | propam
            'lastupdate' => date('Y-m-d H:i:s'),
        ]);
    }

    // Overwrite getId
    public function getById(string $id) {
        $data = parent::getById($id);
        if (!is_null($data)) {
            $data['password'] = $this->decrypt($data['password']);
        }
        return $data;
    }

    // Overwrite save
    public function save(array $data) {
        $data['password'] = $this->encrypt($data['password']);
        return parent::save($data);
    }

}
?>