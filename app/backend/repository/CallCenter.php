<?php 
namespace app\backend\repository;
use core\database\Json;

class CallCenter extends Json {

    public function __construct() {
        parent::__construct('callcenter', 'tb_callcenter', 'id_callcenter');
        parent::setModel([
            'id_callcenter' => 'CALLCENTER',
            'kecelakaan' => '',
            'kemacetan' => '',
            'kebakaran' => '',
            'kejahatan' => '',
            'bencana' => '',
            'bengkel' => '',
            'covid19' => '',
            'donor' => '',
            'medis' => '',
            'pengaduan' => '',
        ]);
    }

}
?>