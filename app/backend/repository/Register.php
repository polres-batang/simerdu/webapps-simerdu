<?php 
namespace app\backend\repository;
use core\database\Mysql;

class Register extends Mysql {

    public function __construct() {
        parent::__construct('mysqldb', 'tb_register', 'id_register');
        parent::setModel([
            'id_register' => $this->setRandomID(),
            'kecamatan_id' => '',
            'keldesa_id' => '',
            'nomer_handphone' => '',
            'nomer_identitas' => '',
            'nama_register' => '',
            'alamat_register' => '',
            'email_register' => '',
            'password' => '',
            'status' => 1,
            'lastupdate' => date('Y-m-d H:i:s'),
        ]);
    }

    // Overwrite getId
    public function getById(string $id) {
        $data = parent::getById($id);
        if (!is_null($data)) {
            $data['password'] = $this->decrypt($data['password']);
        }
        return $data;
    }

    // Overwrite save
    public function save(array $data) {
        $data['nomer_handphone'] = \preg_replace('/^08/', '628', $data['nomer_handphone']);
        $data['password'] = $this->encrypt($data['password']);
        return parent::save($data);
    }

}
?>