<?php 
namespace app\frontend\controller;

class register extends main {

    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = 'Registered User';
        $this->data['breadcrumb'] = $this->getBreadcrumb(['Registered User']);
    }

}

?>