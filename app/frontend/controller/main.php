<?php 
namespace app\frontend\controller;
use core\{WebApp, Helper};
use oauth\{UserAuth};
use app\backend\service\{
    LaporanService
};

class main extends WebApp {

    public function __construct() {
        parent::__construct();
        $laporanService = new LaporanService();
        $this->notifikasi = $laporanService->getNotifikasi();
        $this->auth = UserAuth::getInstance();
        $this->session = $this->auth->getSession();
        $this->api_url = $this->createLink(['api', 'v1']);
        $this->main_url = $this->createLink([$this->getProject(), $this->getClassName()]);
        $this->base_url = $this->createLink([$this->getProject(), $this->getController()]);
        $this->no_image = $this->createLink(['/upload/default/no-user-image.png']);
        $this->ic_logo = $this->createLink(['/asset/image/ic_logo.png']);
        $this->bg_login = $this->createLink(['/asset/image/bg_login.jpg']);
        $this->alarm = $this->createLink(['/asset/sound/zapsplat_emergency_police_siren_isolated_clean_003_63709.mp3']);
        $this->goggle_maps_api = 'AIzaSyCfbg7h6SL3yVS5Ug_CFCHhm_CgxFosyZc';
        $this->google_maps_url = 'https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=false&amp;libraries=geometry&amp;key='.$this->goggle_maps_api;
        $this->data['page_title'] = 'Dashboard';
		$this->data['breadcrumb'] = $this->getBreadcrumb(['Dashboard']);
		$this->data['page'] = 1; // default number page
		$this->data['size'] = 10; // default size contents
        $this->data['pilihan_size'] = $laporanService->getPilihanSizeLimit();

        $this->role = [
            'admin' => ['main', 'monitoring', 'register', 'callcenter', 'laporan', 'broadcast'],
            'propam' => ['main', 'laporan'],
        ];

        $this->requestMapping('GET', '/', function() {

            if (!$this->session->isValid) {
                // $this->redirect([$this->getProject(), 'login']);
                $this->showView([
                    'view' => 'login', 
                    'data' => $this->data, 
                    'template' => 'appui', 
                    'body' => 'style="background-image: url(\''.$this->bg_login.'\'); background-repeat: no-repeat; background-size: cover; height: 100vh;"',
                    'controller' => $this->getClassName(), 
                    'script' => [
                        $this->main_url.'/script',
                    ]
                ]);
            }
            else {
                $level = $this->session->data->level;
                if (in_array($this->getController(), $this->role[$level])) {
                    $view = ($this->session->data->level == 'admin') ? 'index' : 'index.propam';
                    $this->showView([
                        'view' => $view, 
                        'data' => $this->data, 
                        'template' => 'appui', 
                        'body' => 'class="bg-main"',
                        'script' => [
                            $this->main_url.'/config',
                            $this->base_url.'/socket',
                            $this->base_url.'/script',
                        ]
                    ]);
                }
                else {
                    $this->redirect([$this->getProject()]);
                }
            }
        });

        $this->requestMapping('GET', '/config', function() {
            $code = <<<EOF
            const api_url = "$this->api_url";
            const requestHeader = {
                "Authorization" : "Bearer {$this->session->token}"
            };
            const showMessage = function(status, message) {
                $.bootstrapGrowl("<p>"+message+"</p>", {
                    type: status,
                    delay: 3000,
                    allow_dismiss: true,
                    offset: {from: "top", amount: 20}
                });
            }
            EOF;
            $this->showView(['code' => $code], HELPER::$contentType['js']);
        });

        $this->requestMapping('GET', '/script', function() {
            $this->showView(['view' => 'script', 'data' => $this->data], HELPER::$contentType['js']);
        });

        $this->requestMapping('GET', '/socket', function() {
            $this->showView(['view' => 'socket', 'data' => $this->data, 'controller' => $this->getClassName()], HELPER::$contentType['js']);
        });

        $this->requestMapping('GET', '/logout', function() {
            $this->auth->checkout();
            $this->redirect([$this->getProject()]);
        });
    }

    private function getClassName() {
        $path = explode('\\', __CLASS__);
        return end($path);
    }

    protected function getBreadcrumb(array $page_title) {
        $page_title = array_map(function($v) {
            return '<li><a href="'.$this->base_url.'">'.strtoupper($v).'</a></li>';
        }, $page_title);
        
        return '<li>'.$this->settings->web['title'].'</li>'.implode('', $page_title);
    }

    public function getPreloader() {
        $code = <<<EOF
        <div class="preloader">
            <div class="inner">
                <div class="preloader-spinner themed-background hidden-lt-ie10"></div>
                <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
            </div>
        </div>
        EOF;

        $this->showView(['code' => $code]);
    }

    public function getHeader() {
        $this->data['logo'] = $this->createLink(['/asset/image/ic_logo2.png']);
        $this->data['user'] = strtoupper($this->session->data->fullname);
        $this->showView(['view' => 'header', 'data' => $this->data, 'controller' => $this->getClassName()]);
    }

    public function getSidebar() {
        $view = ($this->session->data->level == 'admin') ? 'sidebar' : 'sidebar.propam';
        $this->showView(['view' => $view, 'data' => $this->data, 'controller' => $this->getClassName()]);
    }

    public function getModal() {
        $this->showView(['view' => 'modal', 'data' => $this->data, 'controller' => $this->getClassName()]);
    }

    // public function showError(string $message, $code = 400) {
    //     $data = [
    //         'message' => 'Halaman '.$this->getActiveUrl().', tidak ditemukan', 
    //         'code' => $code
    //     ];
    //     $this->showView([
    //         'view' => 'error', 
    //         'code' => $code, 
    //         'data' => $data, 
    //         'template' => 'appui',
    //         'controller' => $this->getClassName(), 
    //     ]);
    // }

}

?>