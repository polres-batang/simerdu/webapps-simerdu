<?php 
namespace app\frontend\controller;
use core\{Helper};
use app\backend\service\{
    LaporanService
};

class laporan extends main {

    public function __construct() {
        parent::__construct();
        $laporanService = new LaporanService();
        $this->data['page_title'] = 'Laporan';
        $this->data['breadcrumb'] = $this->getBreadcrumb(['Laporan']);
		$this->data['pilihan_layanan'] = ['' => ['text' => 'SEMUA LAYANAN']] + $laporanService->getPilihanLayanan();
		$this->data['pilihan_status'] = ['' => ['text' => 'SEMUA STATUS']] + $laporanService->getStatusLaporan();

        $this->requestMapping('GET', '/detail/{id}', function($id) use ($laporanService) {
            $detail_laporan = $laporanService->getDetailLaporan($id);
            unset($this->data['pilihan_status']['']);
            $this->data['page_title'] = 'Detail Laporan';
            $this->data['breadcrumb'] = $this->getBreadcrumb(['Monitoring', 'Detail Laporan']);
            $this->data['detail_laporan'] = $detail_laporan;
            $this->showView([
                'view' => 'detail', 
                'data' => $this->data, 
                'template' => 'appui', 
                'script' => [
                    $this->main_url.'/config',
                    $this->base_url.'/detail-script',
                ]
            ]);
        });

        $this->requestMapping('GET', '/detail-script', function() {
            $this->showView(['view' => 'detail-script', 'data' => $this->data], HELPER::$contentType['js']);
        });
    }

}

?>