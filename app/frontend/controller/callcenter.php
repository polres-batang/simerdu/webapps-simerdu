<?php 
namespace app\frontend\controller;
use app\backend\service\{
    CallCenterService
};

class callcenter extends main {

    public function __construct() {
        parent::__construct();
        $callCenterService = new CallCenterService();
        $data_form = $callCenterService->getForm('CALLCENTER');
        $this->data['page_title'] = 'Call Center';
        $this->data['breadcrumb'] = $this->getBreadcrumb(['Call Center']);
        $this->data['data_form'] = $data_form['form'];
    }

}

?>