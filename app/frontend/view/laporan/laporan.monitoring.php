<style>
.d-none { display: none;}
</style>
<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-mini">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<!-- Get Started Block -->
				<div id="monitoring" class="block full">
					<div class="block-title">
						<h2>Emergency Location</h3>
						<div class="block-options pull-left">
							<button class="btn btn-effect-ripple btn-default btn-reload" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Reload"><i class="fa fa-refresh"></i></button>
						</div>
					</div>

					<form class="form-laporan form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<?= $this->html::inputKey('id_monitoring', $detail_laporan['id_monitoring']) ?>
						<?= $this->html::inputKey('nama_register', $detail_laporan['nama_register']) ?>
						<?= $this->html::inputKey('action', $detail_laporan['action']) ?>
						<?= $this->html::inputKey('latitude', $detail_laporan['latitude']) ?>
						<?= $this->html::inputKey('longitude', $detail_laporan['longitude']) ?>
						<?= $this->html::inputKey('status', $detail_laporan['status']) ?>
						<div class="form-group form-actions">
							<div class="col-sm-2">
								<a class="btn btn-effect-ripple btn-primary btn-action" href="whatsapp://send?text=<?= rawurlencode($detail_laporan['message']) ?>"><i class="fa fa-whatsapp"></i> Tindak Lanjuti</a>
							</div>
						</div>
					</form>

					<div id="mapView" style="height:400px; width:100%;"></div>
					<?php //$this->showArray($detail_laporan) ?>

				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->