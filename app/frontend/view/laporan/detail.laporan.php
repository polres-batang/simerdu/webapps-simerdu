<style>
.d-none { display: none;}
</style>
<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<!-- Get Started Block -->
				<div id="laporan" class="block full">
					<div class="block-title">
						<h2>
							Laporan: <?= $detail_laporan['nama_layanan'] ?>
							<span style="display: block;font-size: 12px;font-weight: normal;">Tanggal: <?= $detail_laporan['tanggal_laporan']; ?></span>
						</h2>
						<div class="block-options pull-right">
						<span class="label label-<?= $detail_laporan['status_laporan_color']; ?>"><?= $detail_laporan['status_laporan_text']; ?></span>
						</div>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<li class="active"><a href="#laporan_monitoring">Laporan Monitoring</a></li>
							<li><a href="#hasil_tindakan">Hasil Tindakan</a></li>
						</ul>
					</div>

					<!-- Tabs Content -->
					<form id="form-update" method="post" onsubmit="return false;" autocomplete="off">
						<div class="tab-content">
							<div class="tab-pane active" id="laporan_monitoring">
								<div class="row">
									<div class="col-md-7 col-sm-7 col-xs-7">
										<div class="form-group">
											<label for="nama_pelapor">Nama Pelapor</label>
											<?= $this->html::inputText('nama_pelapor', 'text', $detail_laporan['nama_pelapor'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="nomer_identitas">No. Identitas (KTP/SIM)</label>
											<?= $this->html::inputText('nomer_identitas', 'text', $detail_laporan['nomer_identitas'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="nomer_handphone">No. Handphone</label>
											<?= $this->html::inputText('nomer_handphone', 'text', $detail_laporan['nomer_handphone'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="nama_kecamatan">Kecamatan</label>
											<?= $this->html::inputText('nama_kecamatan', 'text', $detail_laporan['nama_kecamatan'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="nama_keldesa">Kelurahan/Desa</label>
											<?= $this->html::inputText('nama_keldesa', 'text', $detail_laporan['nama_keldesa'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="alamat_register">Alamat</label>
											<?= $this->html::inputTextArea('alamat_register', $detail_laporan['alamat_register'], 'class="form-control" rows="8" readonly') ?>
										</div>
									</div>
									<div class="col-md-5 col-sm-5 col-xs-5">
										<div class="form-group">
											<label for="nama_layanan">Jenis Layanan</label>
											<?= $this->html::inputKey('id_laporan', $detail_laporan['id_laporan']) ?>
											<?= $this->html::inputText('nama_layanan', 'text', $detail_laporan['nama_layanan'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="nama_kategori">Kategori</label>
											<?= $this->html::inputText('nama_kategori', 'text', $detail_laporan['nama_kategori'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="kategori_lainnya">Kategori Lainnya</label>
											<?= $this->html::inputText('kategori_lainnya', 'text', $detail_laporan['kategori_lainnya'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="media_laporan_text">Media Laporan</label>
											<?= $this->html::inputText('media_laporan_text', 'text', $detail_laporan['media_laporan_text'], 'class="form-control" readonly') ?>
										</div>
										<div class="form-group">
											<label for="foto_laporan">Foto Laporan</label>
											<a href="<?= $detail_laporan['foto_laporan'] ?>" data-toggle="lightbox-image">
												<img src="<?= $detail_laporan['foto_laporan'] ?>" alt="image">
											</a>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<label for="lokasi_laporan">Lokasi Laporan</label>
										<div id="mapView" style="height:600px; width:100%;" detail-laporan="<?= base64_encode(json_encode($detail_laporan)) ?>"></div><br>
										<button data-toggle="tooltip" title="Delete Data" id="<?= $detail_laporan['id_laporan'] ?>" class="btn btn-effect-ripple btn-sm btn-danger btn-delete" data-message="Yakin data laporan akan dihapus ?"><i class="fa fa-times"></i> Hapus Laporan</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="hasil_tindakan">
								<div class="row">
									<div class="col-md-7 col-sm-7 col-xs-7">
										<div class="form-group">
											<label for="status_laporan">Status Laporan</label>
											<?= $this->html::inputSelect('status_laporan', $pilihan_status, $detail_laporan['status_laporan'], 'class="form-control select-select2" style="width: 100%"') ?>
										</div>
										<div class="form-group">
											<label for="keterangan_tindak_lanjuti">Keterangan Tindak Lanjuti</label>
											<?= $this->html::inputTextArea('keterangan_tindak_lanjuti', $detail_laporan['keterangan_tindak_lanjuti'], 'class="form-control"') ?>
										</div>
										<div class="form-group">
											<label for="keterangan_hasil_laporan">Hasil Laporan</label>
											<?= $this->html::inputTextArea('keterangan_hasil_laporan', $detail_laporan['keterangan_hasil_laporan'], 'class="form-control" required') ?>
										</div>
										<button type="submit" class="btn btn-block btn-primary">Update Laporan</button>
									</div>
									<div class="col-md-5 col-sm-5 col-xs-5">
										<div class="form-group">
											<label for="foto_hasil_laporan">Foto Hasil Laporan</label>
											<input type="file" name="image_result" id="image_result" requireds><br>
											<a href="<?= $detail_laporan['foto_hasil_laporan'] ?>" data-toggle="lightbox-image">
												<img src="<?= $detail_laporan['foto_hasil_laporan'] ?>" onerror="this.src='<?= $this->no_image ?>'" style="width: 100%" alt="image">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					<!-- END Tabs Content -->
					<?php //$this->showArray($detail_laporan); ?>
				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->
<script src="<?= $this->google_maps_url ?>"></script>