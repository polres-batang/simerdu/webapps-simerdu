if ($("#laporan").length > 0) {
	const laporan = new Frameduz("#laporan");
	const dialog = laporan.createDialog("#modal-fade");
	laporan.activeMenu("ul.sidebar-menu a");
	laporan.showTable = function(){
        laporan.loadTable({
            url: api_url+"/laporan/search", 
            data: $(".form-table").serialize(),
			header: requestHeader, 
            onShow: function(content){
                content.find("[data-toggle='tooltip']").tooltip();
                content.find(".btn-edit").each(function(index, element){
                    $(element).on("click", function(e){
                        e.preventDefault();
                        laporan.showForm(this.id);
                    });
                });

				// Initialize Image Lightbox
				content.find('[data-toggle="lightbox-image"]').magnificPopup({type: 'image', image: {titleSrc: 'title'}});

				// Initialize image gallery lightbox
				content.find('[data-toggle="lightbox-gallery"]').each(function(){
					$(this).magnificPopup({
						delegate: 'a',
						type: 'image',
						gallery: {
							enabled: true,
							navigateByImgClick: true,
							arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%" title="%title%"></button>',
							tPrev: 'Previous',
							tNext: 'Next',
							tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
						},
						image: {titleSrc: 'title'}
					});
				});

                content.find(".btn-delete").each(function(index, element){
                    $(element).on("click", function(e){
                        e.preventDefault();
                        const params = {id: this.id, message: this.getAttribute("data-message")};
                        if(confirm(params.message)){
                            const progress = laporan.createProgress(laporan.table.content);
                            setTimeout(function(){
								laporan.sendRequest("DELETE", {
									url: api_url+"/laporan/"+params.id, 
									header: requestHeader, 
									onSuccess: function(response) {
										// console.log(response);
										progress.remove();
										laporan.showTable();
										showMessage("info", response.message);
									},
									onError: function(error) {
										// console.log(error);
										progress.remove();
										showMessage("danger", error.message);
									}
								});
                            }, 1000);
                        }
                    });
                });
            },
            onPage: function(page_number){
                //console.log(page_number);
                $(".form-table").find("#page").val(page_number);
                laporan.showTable();
            }
        });
        
        return false;
    }

	laporan.showForm = function(id){
		laporan.loadForm({
			url: api_url+"/laporan/form/"+id, 
			header: requestHeader, 
            onShow: function(form_content, data){
				const form = data.form;
				laporan.createForm(form_content, {
					id_laporan: laporan.formel.key("id_laporan", form.id_laporan),
					register_id: laporan.formel.select("register_id", data.pilihan_user, form.register_id).attr("required", true).addClass("form-control select-select2"),
					tanggal_laporan: laporan.formel.input("tanggal_laporan", "text", form.tanggal_laporan).attr("required", true).addClass("form-control input-datepicker"),
					latitude: laporan.formel.input("latitude", "text", form.latitude).attr("required", true).addClass("form-control"),
					longitude: laporan.formel.input("longitude", "text", form.longitude).attr("required", true).addClass("form-control"),
					jenis_layanan: laporan.formel.select("jenis_layanan", data.pilihan_layanan, form.jenis_layanan).attr("required", true).addClass("form-control select-select2"),
					media_laporan: laporan.formel.select("media_laporan", data.pilihan_media, form.media_laporan).attr("required", true).addClass("form-control select-select2"),
					status_laporan: laporan.formel.select("status_laporan", data.pilihan_status, form.status_laporan).attr("required", true).addClass("form-control select-select2"),
					callcenter: laporan.formel.key("callcenter", form.callcenter),
					foto_laporan: laporan.formel.key("foto_laporan", form.foto_laporan),
				});

				form_content.find(".select-select2").css({width: "100%"}).select2({dropdownParent: $("#modal-fade .modal-content")});
				form_content.find(".input-datepicker").css({width: "100%"}).datepicker({format: "yyyy-mm-dd"});
				form_content.find("#jenis_layanan").on("change", function(e) {
					e.preventDefault();
					form_content.find("#callcenter").val(data.pilihan_layanan[this.value].phone);
				});

				dialog.title.html(data.title);
				dialog.body.html(form_content);
				dialog.submit.attr("form", "form-input");
				dialog.modal("show");
			},
			onSubmit: function(form_content){
				const progress = laporan.createProgress(dialog.content);
				setTimeout(function(){
					laporan.sendMultipart("POST", {
						url: api_url+"/laporan", 
						// data: JSON.stringify(form_content.serializeObject()),
						data: form_content[0], // multipart
						header: requestHeader, 
						onSuccess: function(response) {
							// console.log(response);
							sendMessage("monitor_laporan");
							progress.remove();
							laporan.showTable();
							dialog.modal("hide");
							showMessage("info", response.message);
						},
						onError: function(error) {
							// console.log(error);
							progress.remove();
							showMessage("danger", error.message);
						}
					});
				}, 500);
			}
		})
    }
	
	laporan.modul.find(".filter-table").on("change", function(e){
        e.preventDefault();
        $(".form-table").find("#page").val(1);
        laporan.showTable();
    });

    laporan.modul.find(".btn-form").on("click", function(e){
        e.preventDefault();
        laporan.showForm(this.id);
    });

    laporan.modul.find(".btn-reload").on("click", function(e){
        e.preventDefault();
        laporan.showTable();
    });

	laporan.showTable();
}

