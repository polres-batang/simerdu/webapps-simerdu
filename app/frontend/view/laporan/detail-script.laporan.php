const detail_laporan = JSON.parse(window.atob(mapView.getAttribute("detail-laporan")));
// console.log(detail_laporan);
let map = null;
let borderline = null;
let marker = null;

if ($("#laporan").length > 0) {
	const laporan = new Frameduz("#laporan");
	const dialog = laporan.createDialog("#modal-fade");
	laporan.activeMenu("ul.sidebar-menu a");

	laporan.showGoogleMap = function() {
		const progress = laporan.createProgress(laporan.modul.find("#mapView"));
		const mapView = document.getElementById("mapView");
		setTimeout(function(){
			laporan.sendRequest("GET", {
				url: api_url+"/monitoring/maps", 
				header: requestHeader, 
				onSuccess: function(response) {
					console.log(response);
					console.clear();
					progress.remove();
					const batang_maps = response.data;
					const location = new google.maps.LatLng(detail_laporan.latitude, detail_laporan.longitude);
					const infoWindow = new google.maps.InfoWindow({
						content: `
						<h4><strong>LAPORAN ${detail_laporan.nama_layanan}</strong></h4>
						<a href="whatsapp://send?text=${detail_laporan.send_whatsapp}" class="btn btn-effect-ripple btn-primary btn-block"><i class='fa fa-whatsapp'></i> Share Whatsapp</a>
						`,
						position: location
					});

					map = new google.maps.Map(mapView, {
						center: location,
						zoom: 11,
						mapTypeId: "roadmap",
					});

					borderline = new google.maps.Polygon({
						path: batang_maps.borderline.map(function(val){
							return new google.maps.LatLng(val[0], val[1])
						}),
						strokeColor: "#FF0000",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: "#000000",
						fillOpacity: 0.5,
						clickable: false,
						map: map
					});

					marker = new google.maps.Marker({
						position: location,
						map: map,
					});

					marker.addListener("click", function() {
						infoWindow.open(map, marker);
					});

					infoWindow.open(map, marker);
				},
				onError: function(error) {
					console.log(error);
					progress.remove();
					mapView.innerHTML = "<H1>"+error.message+"</H1>";
				}
			});
		}, 1000);
	};

	form_update = laporan.modul.find("#form-update");
	form_update.on("submit", function(e) {
		e.preventDefault();
		const params = $(this).serializeObject();
		const progress = laporan.createProgress(form_update);
		console.log(params);
		setTimeout(function(){
			laporan.sendMultipart("POST", {
				url: api_url+"/laporan/"+detail_laporan.id_laporan, 
				data: form_update[0], // multipart
				header: requestHeader, 
				onSuccess: function(response) {
					// console.log(response);
					progress.remove();
					window.location.reload();
					showMessage("info", response.message);
				},
				onError: function(error) {
					// console.log(error);
					progress.remove();
					showMessage("danger", error.message);
				}
			});
		}, 500);
	});

	laporan.modul.find(".btn-delete").on("click", function(e){
        e.preventDefault();
        const params = {id: this.id, message: this.getAttribute("data-message")};
		if(confirm(params.message)){
			const progress = laporan.createProgress(form_update);
			setTimeout(function(){
				laporan.sendRequest("DELETE", {
					url: api_url+"/laporan/"+params.id, 
					header: requestHeader, 
					onSuccess: function(response) {
						// console.log(response);
						progress.remove();
						window.location = "<?= $this->base_url ?>"
						showMessage("info", response.message);
					},
					onError: function(error) {
						// console.log(error);
						progress.remove();
						showMessage("danger", error.message);
					}
				});
			}, 1000);
		}
    });

	laporan.showGoogleMap();
}

