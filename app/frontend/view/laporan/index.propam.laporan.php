<style>
.d-none { display: none;}
</style>
<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<!-- Get Started Block -->
				<div id="laporan" class="block full">
					<div class="block-title">
						<h2>Total Laporan : <span class="total-data">0</span> Data</h3>
						<div class="block-options pull-left">
							<button class="btn btn-effect-ripple btn-default btn-reload" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Reload"><i class="fa fa-refresh"></i></button>
						</div>
						<div class="block-options pull-right d-none">
							<button id="create" class="btn btn-effect-ripple btn-default btn-form" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Tambah"><i class="fa fa-plus"></i></button>
						</div>
					</div>

					<form class="form-table form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<?= $this->html::inputKey('page', $page) ?>
						<?= $this->html::inputKey('size', $size) ?>
						<div class="form-group form-actions">
							<div class="col-sm-5">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-search"></i>
									</span>
									<?= $this->html::inputText('cari', 'text', '', 'class="form-control filter-table" placeholder="Cari berdasarkan nomer handphone/nama user ..."') ?>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="input-group">
									<span class="input-group-addon">
										Jenis Layanan
									</span>
									<?= $this->html::inputSelect('layanan', $pilihan_layanan, '', 'class="form-control filter-table select-select2"') ?>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
									<span class="input-group-addon">
										Status
									</span>
									<?= $this->html::inputSelect('status', $pilihan_status, '', 'class="form-control filter-table select-select2"') ?>
								</div>
							</div>
						</div>
					</form>

					<div class="fztable-query"></div>

					<div class="fztable-content">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<th style="width: 50px;">#</th>
									<th style="width: 150px;">Foto</th>
									<th>Pelapor</th>
									<th>Laporan</th>
									<th>Status</th>
									<th></th>
								</thead>
								<tbody>
									<tr>
										<td>#{number}</td>
										<td>
											<div class="gallery-image-container animation-fadeInQuick2" style="width: 150px;" data-category="travel">
												<img _src="{foto_laporan}" onerror="this.src='<?= $this->no_image ?>'" style="width: 150px;" alt="">
												<a href="{foto_laporan}" class="gallery-image-options" data-toggle="lightbox-image" title=""></a>
											</div>
										</td>
										<td>
											<h5><strong>{nama_pelapor}</strong></h5>
											<i class="fa fa-fw fa-mobile text-info"></i> +{nomer_handphone}<br>
											<i class="fa fa-fw fa-map-marker text-success"></i> {alamat_pelapor}<br>
										</td>
										<td>
											<blockquote class="pull-left" style="margin: 0; padding: 10px;">
											<h5><strong>{nama_layanan}</strong></h5>
											<p style="font-size: 13px;">{nama_kategori}</p>
												<small>
													<em>{media_laporan_text}</em><br>
													<span class="pull-right">Tanggal {tanggal_laporan}</span>
												</small>
											</blockquote>
										</td>
										<td>
											<span class="label label-{status_laporan_color}">{status_laporan_text}</span>
										</td>
										<td>
											<a href="<?= $this->base_url ?>/detail/{id_laporan}" target="_blank" class="btn btn-effect-ripple btn-sm btn-primary"><i class="fa fa-eye"></i> Detail Laporan</a>
											<button data-toggle="tooltip" title="Edit Data" id="{id_laporan}" class="btn btn-effect-ripple btn-sm btn-success btn-edit d-none"><i class="fa fa-pencil"></i></button>
											<button data-toggle="tooltip" title="Delete Data" id="{id_laporan}" class="btn btn-effect-ripple btn-sm btn-danger btn-delete d-none" data-message="Yakin data laporan akan dihapus ?"><i class="fa fa-times"></i></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="fztable-paging" style="border-top:2px solid #ccc;">
							<ul class="pagination pagination-sm">
								<li><a href="javascript:void(0)" page-number="">{page}</a></li>
							</ul>
						</div>
					</div>

					<form method="post" class="fzform-content form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="register_id">Nama Pelapor</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="id_laporan"></span>
								<span data-form-object="register_id"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="tanggal_laporan">Tanggal Laporan</label>
							<div class="col-md-8 col-sm-8">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
									<span data-form-object="tanggal_laporan"></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="latitude">Koord. Latitude</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="latitude"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="longitude">Koord. Longitude</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="longitude"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="jenis_layanan">Jenis Layanan</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="jenis_layanan"></span>
								<span data-form-object="callcenter"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="media_laporan">Media Laporan</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="media_laporan"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="status_laporan">Status Laporan</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="status_laporan"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="foto_laporan">Foto Laporan</label>
							<div class="col-md-8 col-sm-8">
							<span data-form-object="foto_laporan"></span>
								<input type="file" name="image" id="image">
							</div>
						</div>
					</form>
				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->