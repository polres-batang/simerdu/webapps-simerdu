let map = null;
let borderline = null;
let markers = [];

const alarm = document.createElement("audio");
alarm.setAttribute("src", "<?= $this->alarm ?>");
alarm.setAttribute("type", "audio/mpeg");
alarm.loop = true;
alarm.stop = function() {
	// Stop alarm
	if (alarm.currentTime > 0) {
		alarm.pause();
		alarm.currentTime = 0;
	}
}

alarm.restart = function() {
	// Play alarm
	if (this.currentTime == 0 & markers.length > 0) {
		this.play();
	}
}

$(document).on("hidden.bs.modal", ".modal", function () {
	$(document).find(".btn-reload").trigger("click");
});

if ($("#monitoring").length > 0) {
	const monitoring = new Frameduz("#monitoring");
	const dialog = monitoring.createDialog("#modal-fade");
	monitoring.activeMenu("ul.sidebar-menu a");

	monitoring.showGoogleMap = function() {
		const progress = monitoring.createProgress(monitoring.modul.find("#mapView"));
		const mapView = document.getElementById("mapView");
		setTimeout(function(){
			monitoring.sendRequest("GET", {
				url: api_url+"/monitoring/maps", 
				header: requestHeader, 
				onSuccess: function(response) {
					console.log(response);
					console.clear();
					progress.remove();
					const batang_maps = response.data;
					map = new google.maps.Map(mapView, {
						center: new google.maps.LatLng(batang_maps.latitude, batang_maps.longitude),
						zoom: 11,
						// mapTypeId: "satellite",
						// mapTypeId: "hybrid",
						// mapTypeId: "terrain",
						mapTypeId: "roadmap",
					});

					borderline = new google.maps.Polygon({
						path: batang_maps.borderline.map(function(val){
							return new google.maps.LatLng(val[0], val[1])
						}),
						strokeColor: "#FF0000",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: "#000000",
						fillOpacity: 0.5,
						clickable: false,
						map: map
					});

					monitoring.loadNotifikasi();	
				},
				onError: function(error) {
					console.log(error);
					progress.remove();
					mapView.innerHTML = "<H1>"+error.message+"</H1>";
				}
			});
		}, 1000);
	};

	monitoring.loadNotifikasi = function() {
		monitoring.clearMarker();
		alarm.stop();
		const progress = monitoring.createProgress(monitoring.modul.find("#mapView"));
		setTimeout(function(){
			monitoring.sendRequest("GET", {
				url: api_url+"/monitoring/notifikasi", 
				header: requestHeader, 
				onSuccess: function(response) {
					console.log(response);
					console.clear();
					progress.remove();
					monitoring.modul.find("#total_laporan").html(response.data.count);
					$.each(response.data.value, function(k, v) {
						markers.push(monitoring.showMarker(v));
						// console.log(markers)
					});
				},
				onError: function(error) {
					console.log(error);
				}
			});
		}, 1000);
	};

	monitoring.detailNotifikasi = function(id){
		monitoring.loadForm({
			url: api_url+"/monitoring/notifikasi/"+id, 
			header: requestHeader, 
            onShow: function(form_content, data){
				monitoring.createForm(form_content, {
					nama_pelapor: monitoring.formel.input("nama_pelapor", "text", data.nama_pelapor).attr("readonly", true).addClass("form-control"),
					nomer_handphone: monitoring.formel.input("nomer_handphone", "text", data.nomer_handphone).attr("readonly", true).addClass("form-control"),
					tanggal_laporan: monitoring.formel.input("tanggal_laporan", "text", data.tanggal_laporan).attr("readonly", true).addClass("form-control"),
					nama_layanan: data.nama_layanan,
					nama_kategori: data.nama_kategori,
					media_laporan: data.media_laporan_text,
					keterangan_tindak_lanjuti: monitoring.formel.textArea("keterangan_tindak_lanjuti", data.keterangan_tindak_lanjuti).attr("required", false).addClass("form-control"),
					foto_laporan: $("<img>").attr("src", data.foto_laporan).css({width: "100%", height: "100%", "min-width": "0px!important"}),
					send_whatsapp: $("<a>").attr("href", "whatsapp://send?text="+data.send_whatsapp).addClass("btn btn-effect-ripple btn-primary btn-block").html("<i class='fa fa-whatsapp'></i> Tindak Lanjuti (Share Whatsapp)"),
				});

				dialog.title.html("Detail Laporan");
				dialog.body.html(form_content);
				dialog.footer.hide();
				//dialog.setSize("large");
				dialog.modal("show");
				console.clear();
			},
			onSubmit: function(form_content){}
		})
    }

	monitoring.showMarker = function(data) {
		if (!data) {
			return;
		}

		const marker = new google.maps.Marker({
			position: new google.maps.LatLng(data.latitude, data.longitude),
			animation: google.maps.Animation.DROP,
			optimized: false,
			icon: "/asset/image/sirine.gif",
			map: map
		});

		marker.addListener("click", function() {
			monitoring.detailNotifikasi(data.id_laporan);
			this.setMap(null);
			markers.pop();
			alarm.stop();
		});

		// Play Alarm
		alarm.play();
		return marker;
	};

	monitoring.clearMarker = function() {
		// console.log(markers.length);
		for (let index = 0; index < markers.length; index++) {
			markers[index].setMap(null);
		}

		markers = [];
	}

	monitoring.modul.find(".btn-action").on("click", function(e){
        e.preventDefault();
        alarm.stop();
    });

    monitoring.modul.find(".btn-reload").on("click", function(e){
        e.preventDefault();
        monitoring.loadNotifikasi();
    });

	monitoring.showGoogleMap();
}

