<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-mini">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<!-- Get Started Block -->
				<div id="monitoring" class="block full">
					<div class="block-title">
						<h2>Emergency Location: <span id="total_laporan">0</span> Laporan</h3>
						<div class="block-options pull-left">
							<button class="btn btn-effect-ripple btn-default btn-reload" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Reload"><i class="fa fa-refresh"></i></button>
						</div>
					</div>

					<form class="form-laporan form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<div class="form-group form-action">
							<div class="col-sm-2">
								<button class="btn btn-effect-ripple btn-primary btn-action" style="display: none;"><i class="fa fa-stop"></i> Stop</button>
							</div>
						</div>
					</form>

					<div id="mapView" style="height:600px; width:100%;"></div>
					<?php //$this->showArray($detail_laporan) ?>

					<style>
						.widget-image > img { min-width: 0!important; }
					</style>
					<form method="post" class="fzform-content form-bordered" onsubmit="return false;" autocomplete="off">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<div class="widget-image">
										<span data-form-object="foto_laporan"></span>
										<div class="widget-image-content">
											<div class="pull-right text-light-op"><strong><span data-form-object="media_laporan"></span></strong></div>
											<h2 class="widget-heading text-light"><strong><span data-form-object="nama_layanan"></span></strong></h2>
											<h3 class="widget-heading text-light-op"><span data-form-object="nama_kategori"></span></h3>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="nama_pelapor">Nama Pelapor</label>
									<span data-form-object="nama_pelapor"></span>
								</div>
								<div class="form-group">
									<label for="nomer_handphone">No. Handphone</label>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-mobile"></i>
										</span>
										<span data-form-object="nomer_handphone"></span>
									</div>
								</div>
								<div class="form-group">
									<label for="tanggal_laporan">Tanggal Laporan</label>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</span>
										<span data-form-object="tanggal_laporan"></span>
									</div>
								</div>
								<div class="form-group" style="display: none;">
									<label for="keterangan_tindak_lanjuti">Keterangan Tindak Lanjuti (Optional)</label>
									<span data-form-object="keterangan_tindak_lanjuti"></span>
								</div>
								<div style="padding: 20px;">
									<span data-form-object="send_whatsapp"></span>
								</div>
							</div>
						</div>
					</form>

				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->
<script src="<?= $this->google_maps_url ?>"></script>