<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<!-- Get Started Block -->
				<div id="callcenter" class="block fulls">
					<div class="block-title">
						<h2>Total Call Center : <span class="total-data"><?= count($data_form) ?></span> Data</h3>
						<div class="block-options pull-right">
							<button form="form-input" class="btn btn-effect-ripple btn-primary btn-update" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Update Data"><i class="fa fa-check"></i> Update Data</button>
						</div>
					</div>

					<form id="form-input" class="form-table form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<div class="form-group form-actions" style="padding: 15px;">
							<div class="row">
								<?php foreach ($data_form as $key => $val): ?>

								<div class="col-sm-6 col-md-4 col-lg-3">
									<a href="javascript:void(0)" class="widget">
										<div class="widget-content themed-background-info text-light-op text-center">
											<div class="widget-icon">
												<img src="<?= $val['icon'] ?>" alt="avatar" style="width: 40px;">
											</div>
										</div>
										<div class="widget-content text-dark text-center">
											<strong><?= $val['text'] ?></strong><br><br>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-phone"></i>
												</span>
												<?= $this->html::inputText($key, 'text', $val['phone'], 'class="form-control phone-number" placeholder="62xxx" required') ?>
											</div>
										</div>
									</a>
								</div>
								
								<?php endforeach; ?>
							</div>
							<?php //$this->showArray($data_form) ?>
						</div>
					</form>
				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->