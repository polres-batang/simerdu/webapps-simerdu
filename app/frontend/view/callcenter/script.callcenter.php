if ($("#callcenter").length > 0) {
	const callcenter = new Frameduz("#callcenter");
	callcenter.activeMenu("ul.sidebar-menu a");
	
	form_input = callcenter.modul.find("#form-input");
	form_input.on("submit", function(e) {
		e.preventDefault();
		const auth_url = $(this).attr("action");
		const params = $(this).serializeObject();
		const progress = callcenter.createProgress(form_input);
		setTimeout(function(){
			callcenter.sendRequest("POST", {
				url: api_url+"/callcenter", 
				data: params,
				header: requestHeader, 
				onSuccess: function(response) {
					// console.log(response);
					progress.remove();
					showMessage("info", response.message);
				},
				onError: function(error) {
					// console.log(error);
					progress.remove();
					showMessage("danger", error.message);
				}
			});
		}, 1000);
	});

	form_input.find(".phone-number").on("keypress", function(e) {
		// console.log(e.keyCode);
		if (e.keyCode !== 13) {
			const patern = /^[0-9]$/;
			return patern.test(e.key);
		}
	});

}
