<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<!-- Get Started Block -->
				<div id="broadcast" class="block full">
					<div class="block-title">
						<h2>Total Broadcast : <span class="total-data">0</span> Data</h3>
						<div class="block-options pull-left">
							<button class="btn btn-effect-ripple btn-default btn-reload" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Reload"><i class="fa fa-refresh"></i></button>
						</div>
					</div>

					<form class="form-table form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<?= $this->html::inputKey('page', $page) ?>
						<?= $this->html::inputKey('size', $size) ?>
						<div class="form-group form-actions">
							<div class="col-sm-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-search"></i>
									</span>
									<?= $this->html::inputText('cari', 'text', '', 'class="form-control filter-table" placeholder="Cari berdasarkan judul/sumber broadcast ..."') ?>
								</div>
							</div>
							<div class="col-sm-2 d-none">
								<button type="button" id="create" class="btn btn-effect-ripple btn-primary btn-form"><i class="fa fa-plus"></i> Tambah Data</button>
							</div>
						</div>
					</form>

					<div class="fztable-query"></div>

					<div class="fztable-content">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<th style="width: 50px;">#</th>
									<th style="width: 250px;">Gambar</th>
									<th>Broadcast</th>
									<th style="width: 100px;"></th>
								</thead>
								<tbody>
									<tr>
										<td>#{number}</td>
										<td>
											<a href="{gambar_broadcast}" data-toggle="lightbox-image">
												<img _src="{gambar_broadcast}" alt="">
											</a>
										</td>
										<td>
											<blockquote class="pull-left" style="margin: 0; padding: 10px;">
											<h5><strong>{judul_broadcast}</strong></h5>
											<p style="font-size: 13px;">{short_broadcast}</p>
												<small>
													Sumber: {sumber_broadcast}<br>
													<span class="pull-right">{tanggal_broadcast}</span>
												</small>
											</blockquote>
										</td>
										<td class="d-none">
											<button data-toggle="tooltip" title="Edit Data" id="{id_broadcast}" class="btn btn-effect-ripple btn-sm btn-success btn-edit"><i class="fa fa-pencil"></i></button>
											<button data-toggle="tooltip" title="Delete Data" id="{id_broadcast}" class="btn btn-effect-ripple btn-sm btn-danger btn-delete" data-message="Yakin data broadcast ini akan dihapus ?"><i class="fa fa-times"></i></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="fztable-paging" style="border-top:2px solid #ccc;">
							<ul class="pagination pagination-sm">
								<li><a href="javascript:void(0)" page-number="">{page}</a></li>
							</ul>
						</div>
					</div>

					<form method="post" class="fzform-content form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="judul_broadcast">Judul Broadcast</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="id_broadcast"></span>
								<span data-form-object="judul_broadcast"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="gambar_broadcast">Gambar Thumbnail</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="gambar_broadcast"></span>
								<input type="file" name="image" id="image">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="sumber_broadcast">Sumber Broadcast</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="sumber_broadcast"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="isi_broadcast">Isi Broadcast</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="isi_broadcast"></span>
							</div>
						</div>
					</form>
				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->