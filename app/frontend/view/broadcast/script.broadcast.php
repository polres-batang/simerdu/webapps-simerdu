if ($("#broadcast").length > 0) {
	const broadcast = new Frameduz("#broadcast");
	const dialog = broadcast.createDialog("#modal-fade");
	broadcast.activeMenu("ul.sidebar-menu a");
	broadcast.showTable = function(){
        broadcast.loadTable({
            url: api_url+"/broadcast/search", 
            data: $(".form-table").serialize(),
			header: requestHeader, 
            onShow: function(content){
                content.find("[data-toggle='tooltip']").tooltip();
                content.find(".btn-edit").each(function(index, element){
                    $(element).on("click", function(e){
                        e.preventDefault();
                        broadcast.showForm(this.id);
                    });
                });

				// Initialize Image Lightbox
				content.find('[data-toggle="lightbox-image"]').magnificPopup({type: 'image', image: {titleSrc: 'title'}});

				// Initialize image gallery lightbox
				content.find('[data-toggle="lightbox-gallery"]').each(function(){
					$(this).magnificPopup({
						delegate: 'a',
						type: 'image',
						gallery: {
							enabled: true,
							navigateByImgClick: true,
							arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%" title="%title%"></button>',
							tPrev: 'Previous',
							tNext: 'Next',
							tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
						},
						image: {titleSrc: 'title'}
					});
				});

                content.find(".btn-delete").each(function(index, element){
                    $(element).on("click", function(e){
                        e.preventDefault();
                        const params = {id: this.id, message: this.getAttribute("data-message")};
                        if(confirm(params.message)){
                            const progress = broadcast.createProgress(broadcast.table.content);
                            setTimeout(function(){
								broadcast.sendRequest("DELETE", {
									url: api_url+"/broadcast/"+params.id, 
									header: requestHeader, 
									onSuccess: function(response) {
										// console.log(response);
										progress.remove();
										broadcast.showTable();
										showMessage("info", response.message);
									},
									onError: function(error) {
										// console.log(error);
										progress.remove();
										showMessage("danger", error.message);
									}
								});
                            }, 1000);
                        }
                    });
                });
            },
            onPage: function(page_number){
                //console.log(page_number);
                $(".form-table").find("#page").val(page_number);
                broadcast.showTable();
            }
        });
        
        return false;
    }

	broadcast.showForm = function(id){
		broadcast.loadForm({
			url: api_url+"/broadcast/form/"+id, 
			header: requestHeader, 
            onShow: function(form_content, data){
				const form = data.form;
				broadcast.createForm(form_content, {
					id_broadcast: broadcast.formel.key("id_broadcast", form.id_broadcast),
					judul_broadcast: broadcast.formel.input("judul_broadcast", "text", form.judul_broadcast).attr("required", true).addClass("form-control"),
					// isi_broadcast: broadcast.formel.input("isi_broadcast", "text", form.isi_broadcast).attr("required", true).addClass("form-control"),
					isi_broadcast: broadcast.formel.textArea("isi_broadcast", form.isi_broadcast).attr("required", true).addClass("form-control"),
					sumber_broadcast: broadcast.formel.input("sumber_broadcast", "text", form.sumber_broadcast).attr("required", true).addClass("form-control"),
					gambar_broadcast: broadcast.formel.key("gambar_broadcast", form.gambar_broadcast),
				});

				// form_content.find(".select-select2").css({width: "100%"}).select2({dropdownParent: $("#modal-fade .modal-content")});
				// form_content.find(".input-datepicker").css({width: "100%"}).datepicker({format: "yyyy-mm-dd"});
				// form_content.find("#action").on("change", function(e) {
				// 	e.preventDefault();
				// 	form_content.find("#callcenter").val(data.pilihan_action[this.value].phone);
				// });

				dialog.title.html(data.title);
				dialog.body.html(form_content);
				dialog.submit.attr("form", "form-input");
				dialog.modal("show");
			},
			onSubmit: function(form_content){
				const progress = broadcast.createProgress(dialog.content);
				setTimeout(function(){
					broadcast.sendMultipart("POST", {
						url: api_url+"/broadcast", 
						// data: JSON.stringify(form_content.serializeObject()),
						data: form_content[0], // multipart
						header: requestHeader, 
						onSuccess: function(response) {
							// console.log(response);
							progress.remove();
							broadcast.showTable();
							dialog.modal("hide");
							showMessage("info", response.message);
						},
						onError: function(error) {
							// console.log(error);
							progress.remove();
							showMessage("danger", error.message);
						}
					});
				}, 500);
			}
		})
    }
	
	broadcast.modul.find(".filter-table").on("change", function(e){
        e.preventDefault();
        $(".form-table").find("#page").val(1);
        broadcast.showTable();
    });

    broadcast.modul.find(".btn-form").on("click", function(e){
        e.preventDefault();
        broadcast.showForm(this.id);
    });

    broadcast.modul.find(".btn-reload").on("click", function(e){
        e.preventDefault();
        broadcast.showTable();
    });

	broadcast.showTable();
}
