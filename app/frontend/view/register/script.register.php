if ($("#register").length > 0) {
	const register = new Frameduz("#register");
	const dialog = register.createDialog("#modal-fade");
	register.activeMenu("ul.sidebar-menu a");
	register.showTable = function(){
        register.loadTable({
            url: api_url+"/registers/search", 
            data: $(".form-table").serialize(),
			header: requestHeader, 
            onShow: function(content){
                content.find("[data-toggle='tooltip']").tooltip();
                content.find(".btn-edit").each(function(index, element){
                    $(element).on("click", function(e){
                        e.preventDefault();
                        register.showForm(this.id);
                    });
                });

                content.find(".btn-delete").each(function(index, element){
                    $(element).on("click", function(e){
                        e.preventDefault();
                        const params = {id: this.id, message: this.getAttribute("data-message")};
                        if(confirm(params.message)){
                            const progress = register.createProgress(register.table.content);
                            setTimeout(function(){
								register.sendRequest("DELETE", {
									url: api_url+"/registers/"+params.id, 
									header: requestHeader, 
									onSuccess: function(response) {
										// console.log(response);
										progress.remove();
										register.showTable();
										showMessage("info", response.message);
									},
									onError: function(error) {
										// console.log(error);
										progress.remove();
										showMessage("danger", error.message);
									}
								});
                            }, 1000);
                        }
                    });
                });
            },
            onPage: function(page_number){
                //console.log(page_number);
                $(".form-table").find("#page").val(page_number);
                register.showTable();
            }
        });
        
        return false;
    }

	register.showForm = function(id){
		let form_input = true;
		let form_url = "";
		register.loadForm({
			url: api_url+"/registers/form/"+id, 
			header: requestHeader, 
            onShow: function(form_content, data){
				const form = data.form;
				register.createForm(form_content, {
					id_register: register.formel.key("id_register", form.id_register),
					nomer_handphone: register.formel.input("nomer_handphone", "text", form.nomer_handphone).attr("required", true).addClass("form-control"),
					nomer_identitas: register.formel.input("nomer_identitas", "text", form.nomer_identitas).attr("required", true).addClass("form-control"),
					nama_register: register.formel.input("nama_register", "text", form.nama_register).attr("required", true).addClass("form-control"),
					alamat_register: register.formel.input("alamat_register", "text", form.alamat_register).attr("required", true).addClass("form-control"),
					kecamatan_id: register.formel.select("kecamatan_id", data.pilihan_kecamatan, form.kecamatan_id).attr("required", true).addClass("form-control select-select2"),
					keldesa_id: register.formel.select("keldesa_id", data.pilihan_keldesa, form.keldesa_id).attr("required", true).addClass("form-control select-select2"),
					email_register: register.formel.input("email_register", "text", form.email_register).attr("required", true).addClass("form-control"),
					password: register.formel.input("password", "text", form.password).attr("required", true).addClass("form-control"),
				});

				form_content.find(".select-select2").css({width: "100%"}).select2({dropdownParent: $("#modal-fade .modal-content")});
				form_input = (form.nomer_handphone == "" && form.nomer_identitas == "");
				form_url = (form_input) ? "/registers" : "/registers/"+form.id_register;

				dialog.title.html(data.title);
				dialog.body.html(form_content);
				dialog.submit.attr("form", "form-input");
				dialog.modal("show");
			},
			onSubmit: function(form_content){
				if (form_content.find("#kecamatan_id").val() == "") {
					return alert("Pilih Kecamatan !");
				}
	
				if (form_content.find("#keldesa_id").val() == "") {
					return alert("Pilih Kel/Desa !");
				}
				
				const progress = register.createProgress(dialog.content);
				setTimeout(function(){
					register.sendRequest("POST", {
						url: api_url+form_url, 
						data: JSON.stringify(form_content.serializeObject()),
						header: requestHeader, 
						onSuccess: function(response) {
							// console.log(response);
							progress.remove();
							register.showTable();
							dialog.modal("hide");
							showMessage("info", response.message);
						},
						onError: function(error) {
							// console.log(error);
							progress.remove();
							showMessage("danger", error.message);
						}
					});
				}, 500);
			}
		})
    }
	
	register.modul.find(".filter-table").on("change", function(e){
        e.preventDefault();
        $(".form-table").find("#page").val(1);
        register.showTable();
    });

    register.modul.find(".btn-form").on("click", function(e){
        e.preventDefault();
        register.showForm(this.id);
    });

    register.modul.find(".btn-reload").on("click", function(e){
        e.preventDefault();
        register.showTable();
    });

	register.showTable();
}
