<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<!-- Get Started Block -->
				<div id="register" class="block full">
					<div class="block-title">
						<h2>Total Users : <span class="total-data">0</span> Data</h3>
						<div class="block-options pull-left">
							<button class="btn btn-effect-ripple btn-default btn-reload" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Reload"><i class="fa fa-refresh"></i></button>
						</div>
					</div>

					<form class="form-table form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<?= $this->html::inputKey('page', $page) ?>
						<?= $this->html::inputKey('size', $size) ?>
						<div class="form-group form-actions">
							<div class="col-sm-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-search"></i>
									</span>
									<?= $this->html::inputText('cari', 'text', '', 'class="form-control filter-table" placeholder="Cari berdasarkan nomer handphone/nama user ..."') ?>
								</div>
							</div>
							<div class="col-sm-3">
								<button type="button" id="create" class="btn btn-effect-ripple btn-primary btn-form"><i class="fa fa-plus"></i> Tambah Data</button>
							</div>
						</div>
					</form>

					<div class="fztable-query"></div>

					<div class="fztable-content">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<th>#</th>
									<th>Nama</th>
									<th>No. Hp</th>
									<th>No. Identitas</th>
									<th>Alamat</th>
									<th>Email</th>
									<th></th>
								</thead>
								<tbody>
									<tr>
										<td>{number}</td>
										<td>{nama_register}</td>
										<td>{nomer_handphone}</td>
										<td>{nomer_identitas}</td>
										<td>{alamat_register}</td>
										<td>{email_register}</td>
										<td>
											<button data-toggle="tooltip" title="Edit Data" id="{id_register}" class="btn btn-effect-ripple btn-sm btn-success btn-edit"><i class="fa fa-pencil"></i></button>
											<button data-toggle="tooltip" title="Delete Data" id="{id_register}" class="btn btn-effect-ripple btn-sm btn-danger btn-delete" data-message="Yakin data user {nama_register} akan dihapus ?"><i class="fa fa-times"></i></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="fztable-paging" style="border-top:2px solid #ccc;">
							<ul class="pagination pagination-sm">
								<li><a href="javascript:void(0)" page-number="">{page}</a></li>
							</ul>
						</div>
					</div>

					<form method="post" class="fzform-content form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="nomer_handphone">No. Handphone</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="id_register"></span>
								<span data-form-object="nomer_handphone"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="nomer_identitas">No. Identitas (KTP/SIM)</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="nomer_identitas"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="nama_register">Nama Lengkap</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="nama_register"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="alamat_register">Alamat</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="alamat_register"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="kecamatan_id">Kecamatan</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="kecamatan_id"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="keldesa_id">Kel/Desa</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="keldesa_id"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="email_register">Email</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="email_register"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 col-sm-4 control-label" for="password">Password</label>
							<div class="col-md-8 col-sm-8">
								<span data-form-object="password"></span>
								<span class="help-block">*) Digunakan untuk login ke aplikasi mobile</span>
							</div>
						</div>
					</form>
				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->