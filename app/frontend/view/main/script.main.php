if ($("#login").length > 0) {
	console.clear();
	const login = new Frameduz("#login");
	form_login = login.modul.find("#form-login");
	form_login.on("submit", function(e) {
		e.preventDefault();
		const auth_url = $(this).attr("action");
		const params = $(this).serializeObject();
		const progress = login.createProgress(form_login);
		setTimeout(function(){
			login.sendRequest("POST", {
				url: auth_url, 
				data: params,
				onSuccess: function(response) {
					// console.log(response);
					progress.remove();
					window.location.reload();
				},
				onError: function(error) {
					// console.log(error);
					progress.remove();
					alert(error.message);
				}
			});
		}, 1000);
	});
}

if ($("#dashboard").length > 0) {
	const dashboard = new Frameduz("#dashboard");
	dashboard.activeMenu("ul.sidebar-menu a");
	form_chart = dashboard.modul.find(".form-chart");

	dashboard.showChart = function(kategori, callback) {
		const chart = "chart-"+kategori;
		const progress = dashboard.createProgress($("#"+chart));
		setTimeout(function(){
			dashboard.sendRequest("POST", {
				url: api_url+"/dashboard/statistik/"+kategori, 
				data: form_chart.serializeObject(),
				header: requestHeader, 
				onSuccess: function(response) {
					// console.log(response);
					callback(chart, response.data.statistik);
					progress.remove();
				},
				onError: function(error) {
					// console.log(error);
					progress.remove();
				}
			});
		}, 1000);
		
		// console.clear();
		return false;
	}

	dashboard.generatedChart = function(){
		dashboard.showChart('pengguna', function(chart, data) {
			createBarChart(chart, data, "Total Pengguna");
		});
		dashboard.showChart('layanan', function(chart, data) {
			createBarChart(chart, data, "Total Laporan");
		});
		dashboard.showChart('pengaduan', function(chart, data) {
			createPieChart(chart, data, "Total Laporan");
		});
	}

	dashboard.modul.find(".filter-chart").on("change", function(e){
		e.preventDefault();
		dashboard.modul.find(".btn-reload").trigger("click");
		
	});

	dashboard.modul.find(".btn-reload").on("click", function(e){
        e.preventDefault();
        $(".form-chart").find("#page").val(1);
		dashboard.generatedChart();
    });
	
	dashboard.modul.find("#filterDate").daterangepicker({
		locale: {
			format: "DD/MM/YYYY",
		},
		ranges: {
			'Hari Ini': [moment(), moment()],
			'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
			'30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
			'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
			'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			// 'Semua Tanggal': [new Date("2019"), moment()]
		}
	}, function(start, end, label){
		startDate = start.format("YYYY-MM-DD");
		endDate = end.format("YYYY-MM-DD");
		// console.log(startDate, endDate);
	
		$(".form-chart").find("#start_date").val(startDate);
		$(".form-chart").find("#end_date").val(endDate);
	});
}

const createBarChart = function(id, statistik, description){
    if ($("#"+id).length == 0) return;

    Highcharts.chart(id, {
        chart: {
            type: "bar",
            // inverted: true
        },
        title: {
            text: null
        },
        accessibility: {
            keyboardNavigation: {
                seriesNavigation: {
                    mode: "serialize"
                }
            }
        },
        xAxis: {
            categories: statistik.labels,
        },
        yAxis: {
            title: {
                text: null
            },
            allowDecimals: false,
            min: 0
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: description,
            data: statistik.values
        }],
        credits: {
            enabled: false
        },
    });
}

const createPieChart = function(id, statistik, description){
    if ($("#"+id).length == 0) return;
	let data_pie = [];
	for (let i = 0; i < statistik.labels.length; i++) {
		data_pie.push({
			name: statistik.labels[i],
			y: statistik.values[i]
		})
	}

	// console.log(data_pie);
    Highcharts.chart(id, {
        chart: {
            type: "pie",

            plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
        },
        title: {
            text: null
        },
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
		},
        accessibility: {
			point: {
				valueSuffix: '%'
			}
        },
        plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: false,
				},
				showInLegend: true
			}
        },
        series: [{
			colorByPoint: true,
            name: description,
			data: data_pie
        }],
        credits: {
            enabled: false
        },
    });
}

// Create Gradient Color All Chart
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.color(color).brighten(-0.3).get("rgb")] // darken
            ]
        };
    })
});