<div id="page-wrapper" class="page-loading">
	<?php $this->getPreloader(); ?>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
		<!-- Main Sidebar -->
		<?php $this->getSidebar(); ?>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<?php $this->getHeader(); ?>
			<!-- END Header -->

			<!-- Modal -->
			<?php $this->getModal(); ?>
            <!-- END Modal -->

			<!-- Page content -->
			<div id="page-content">
				<!-- Blank Header -->
				<div class="content-header">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-section">
								<h1><?= $page_title ?></h1>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<div class="header-section">
								<ul class="breadcrumb breadcrumb-top">
									<?= $breadcrumb ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END Blank Header -->

				<?php //$this->showArray($this->session) ?>

				<!-- Get Started Block -->
				<div id="dashboard" class="block full">
					<div class="block-title">
						<h2>Statistik Penggunaan Layanan Aplikasi Simerdu</h3>
						<div class="block-options pull-left">
							<button class="btn btn-effect-ripple btn-default btn-reload" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Reload"><i class="fa fa-refresh"></i></button>
						</div>
					</div>

					<form class="form-chart form-horizontal form-bordered" onsubmit="return false;" autocomplete="off">
						<?= $this->html::inputKey('page', $page) ?>
						<?= $this->html::inputKey('start_date', '') ?>
						<?= $this->html::inputKey('end_date', '') ?>
						<div class="form-group form-actions">
							<div class="col-sm-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-bar-chart-o"></i>
									</span>
									<?= $this->html::inputSelect('size', $pilihan_size, '', 'class="form-control filter-chart select-select2" placeholder="Pilih Size ..."') ?>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
									<?= $this->html::inputText('filterDate', 'text', '', 'class="form-control filter-chart" placeholder="" readonly') ?>
								</div>
							</div>
						</div>
					</form>
					<br>
					<div class="row">
						<div class="col-sm-6">
							<div class="block full">
								<div class="block-title">
									<h2>Jumlah Pengguna Per Kecamatan</h2>
								</div>
								<div id="chart-pengguna" style="height: 500px;"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="block full">
								<div class="block-title">
									<h2>Jumlah Penggunaan Layanan</h2>
								</div>
								<div id="chart-layanan" style="height: 500px;"></div>
							</div>
						</div>
					</div>

					<?php //$this->showArray($statistik_pengguna); ?>
				</div>
				<!-- END Get Started Block -->
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->