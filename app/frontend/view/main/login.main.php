<!-- Login Container -->
<div id="login-container" style="top: 0px;">
    <!-- Login Header -->
    <h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
        <img src="<?= $this->ic_logo ?>" style="width: 300px;" alt=""><br>
    </h1>
    <!-- END Login Header -->

    <!-- Login Block -->
    <div id="login" class="block animation-fadeInQuickInv" style="background-color: #0c096c; filter: opacity(.8); color: #fff;">
        <!-- Login Title -->
        <div class="block-title">
            <h2>Please Login</h2>
        </div>
        <!-- END Login Title -->

        <!-- Login Form -->
        <form id="form-login" action="<?= $this->getBaseUrl().'/oauth/user/auth' ?>" method="post" class="form-horizontal" autocomplete="off">
            <div class="form-group">
                <div class="col-xs-12">
                    <?= $this->html::inputText('username', 'text', '', 'class="form-control" placeholder="Input Username" autofocus required') ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <?= $this->html::inputText('password', 'password', '', 'class="form-control" placeholder="Input Password" required') ?>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-4 text-right">
                    <button type="submit" class="btn btn-effect-ripple btn-sm btn-primary"><i class="fa fa-check"></i> Sign In</button>
                </div>
            </div>
        </form>
        <!-- END Login Form -->
    </div>
    <!-- END Login Block -->

    <!-- Footer -->
    <footer class="text-muted text-center animation-pullUp">
        <!-- <small><a href="/asset/app-release-v3.apk" style="color: white;" target="_blank">Download Mobile App</a></small> -->
    </footer>
    <!-- END Footer -->
</div>
<!-- END Login Container -->