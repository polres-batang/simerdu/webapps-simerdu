<div id="sidebar">
    <!-- Sidebar Brand -->
    <div id="sidebar-brand" class="themed-background">
        <a href="<?= $this->createLink(); ?>" class="sidebar-title">
            <i class="fa fa-cube"></i> <span class="sidebar-nav-mini-hide"><strong>ADMIN PANEL</strong></span>
        </a>
    </div>
    <!-- END Sidebar Brand -->

    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Sidebar Navigation -->
            <ul class="sidebar-menu sidebar-nav">
                <li><a href="<?= $this->createLink() ?>"><i class="gi gi-home sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a></li>
                <li>
                    <a href="<?= $this->createLink(['monitoring']) ?>">
                        <i class="gi gi-display sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Monitoring</span>
                        <span class="badge sidebar-nav-mini-hide" style="position: absolute; top: 10px; right: 10px;"><?= $this->notifikasi['count'] ?></span>
                    </a>
                </li>
                <li><a href="<?= $this->createLink(['register']) ?>"><i class="gi gi-group sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Registered User</span></a></li>
                <li><a href="<?= $this->createLink(['callcenter']) ?>"><i class="gi gi-earphone sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Call Center</span></a></li>
                <li><a href="<?= $this->createLink(['laporan']) ?>"><i class="gi gi-bell sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Laporan</span></a></li>
                <li><a href="<?= $this->createLink(['broadcast']) ?>"><i class="gi gi-bullhorn sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Broadcast Info</span></a></li>
                <li class="sidebar-separator">
                    <i class="fa fa-ellipsis-h"></i>
                </li>
            </ul>
            <!-- END Sidebar Navigation -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->

    <!-- Sidebar Extra Info -->
    <div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide" style="display: none;">
        <div class="text-center">
            <small>Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I" target="_blank">pixelcave</a></small><br>
            <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/RcsdAh" target="_blank">AppUI 2.9</a></small>
        </div>
    </div>
    <!-- END Sidebar Extra Info -->
</div>