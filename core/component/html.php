<?php 
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: Component WebApp
 * @Description : fzstyle.css
 */

namespace core\component;

class HTML {
    
    public static function inputKey($nm_comp, $data){
        return '<input type="hidden" name="'.$nm_comp.'" id="'.$nm_comp.'" value="'.$data.'" />';
    }

    public static function inputText($nm_comp, $type, $data, $opt){
        return '<input type="'.$type.'" id="'.$nm_comp.'" name="'.$nm_comp.'" value="'.$data.'" '.$opt.' />';
    }

    public static function inputTextArea($nm_comp, $data, $opt){
        return '<textarea id="'.$nm_comp.'" name="'.$nm_comp.'" '.$opt.' rows="5">'.$data.'</textarea>';
    }

    public static function inputSelect($nm_comp, $data, $val, $opt){
        $dataArr = array();
        $option = array();
        
        // Cek Data Group
        foreach ($data as $key => $value) {
            $group = isset($value['group']) ? $value['group'] : '';
            if(!empty($group)){
                $dataArr[$group]['detail'][$key] = $value;
            }else{
                $dataArr[$key] = $value;
            }
        }

        foreach ($dataArr as $key => $value) {
            if(isset($value['detail'])){ // Jek jika data group
                $option[] = '<optgroup label="'.$key.'">';
                foreach ($value['detail'] as $keys => $values) {
                    $text = isset($values['text']) ? $values['text'] : '';
                    $desc = isset($values['desc']) ? $values['desc'] : '';
                    if(is_array($val)) $selected = (in_array($key, $val)) ? 'selected' : ''; // Multiple Select
                    else $selected = ($key == $val) ? 'selected' : '';
                    $option[] = '<option data-subtext="'.$desc.'" data-subgroup="true" value="'.$keys.'" '.$selected.'>'.$text.'</option>';
                }
                $option[] = '</optgroup>';
            }else{
                $text = isset($value['text']) ? $value['text'] : '';
                $desc = isset($value['desc']) ? $value['desc'] : '';
                if(is_array($val)) $selected = (in_array($key, $val)) ? 'selected' : ''; // Multiple Select
                else $selected = ($key == $val) ? 'selected' : '';
                $option[] = '<option data-subtext="'.$desc.'" data-subgroup="" value="'.$key.'" '.$selected.'>'.$text.'</option>';
            }
        }

        $elmt = '<select id="'.$nm_comp.'" name="'.$nm_comp.'" '.$opt.'>'.implode('', $option).'</select>';
        return $elmt;        
    }
	
	public static function inputRadio($nm_comp, $data, $val){
		foreach ($data as $key => $value){
            if($key == $val)
                $option[] = '<label style="color: #000; cursor: pointer; margin: 10px;"><input type="radio" id="'.$nm_comp.'" name="'.$nm_comp.'" value="'.$key.'" checked> '.$value['text'].'</label>';
            else
                $option[] = '<label style="color: #000; cursor: pointer; margin: 10px;"><input type="radio" id="'.$nm_comp.'" name="'.$nm_comp.'" value="'.$key .'"> '.$value['text'].'</label>';
        }
        $set = implode('', $option);
        return $set;
	}
	
	public static function inputCheckbox($nm_comp, $data, $val){
		$val = (isset($val)) ? explode(',', $val) : array();
		foreach ($data as $key => $value){
			if(in_array($value, $val))
				$option[] = '<label style="color: #000; cursor: pointer; margin: 5px;"><input type="checkbox" id="'.$nm_comp.'" name="'.$nm_comp.'" value="'.$value.'" checked> '.$value.'</label>';
			else
                $option[] = '<label style="color: #000; cursor: pointer; margin: 5px;"><input type="checkbox" id="'.$nm_comp.'" name="'.$nm_comp.'" value="'.$value.'"> '.$value.'</label>';
		}
        $set = implode('', $option);
        return $set;
	}

    public static function errMsg($str, $class){
        return '<div class="alert alert-'.$class.' alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
					<strong>'.$str.'</strong>
				</div>'."\n";
    }

}
?>