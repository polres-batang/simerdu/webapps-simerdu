/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: Component WebApp
 * @Description : fzscript.js
 */

// console.clear();
console.group("<?= $this->settings->web['title'] ?>");
console.log("Author:", "<?= $this->settings->web['author'] ?>");
console.log("Description:", "<?= $this->settings->web['description'] ?>");
console.groupEnd();

Frameduz = function(id) {
    modul = $(id);
    table = {
        query: modul.find(".fztable-query"),
        content: modul.find(".fztable-content"),
        contentTbody: modul.find(".fztable-content .table tbody"),
        contentTbodyRows: modul.find(".fztable-content .table tbody tr"),
        paging: modul.find(".fztable-paging ul"),
        pagingItem: modul.find(".fztable-paging ul li"),
        totalData: modul.find(".total-data"),
        empty: function() {
            let tableEmpty = null;
            if (modul.find(".fztable-empty").length > 0) {
                tableEmpty = modul.find(".fztable-empty");
            }
            else {
                // tableEmpty = $("<div>").addClass("fztable-empty").html('<div class="alert alert-danger alert-dismissable"><p><strong>Data tidak ditemukan !</strong></p></div>');
                tableEmpty = $("<div>").addClass("fztable-empty");
                modul.append(tableEmpty);
            }

            return tableEmpty;
        },
        loader: function() {
            let tableLoader = null;
            if (modul.find(".fztable-loader").length > 0) {
                tableLoader = modul.find(".fztable-loader");
            }
            else {
                tableLoader = $("<div>").addClass("fztable-loader").html('<div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div>');
                modul.append(tableLoader);
            }
            
            return tableLoader;
        }
    }

    this.modul = modul;
    this.table = table;
    this.form = modul.find(".fzform-content").clone().show();
}

// Active Menu by Url
Frameduz.prototype.activeMenu = function(obj) {
    var macthUrl = [];
    var pageUrl = window.location.toString();
    $(obj).each(function(k, v){
        if(pageUrl.match(this.href)) macthUrl.push(this);
    });
    var element = $(macthUrl[macthUrl.length - 1]);
    element.addClass("active").parent().addClass("active").parent().addClass("show").parent().addClass("active open");
}

// Ajax Request json, form-urlencoded, query params
Frameduz.prototype.sendRequest = function(type, params){
    $.ajax({
        type: type,
        url: params.url,
        data: params.data,
        headers: params.header,
        dataType: "json",
        async: false,
        success: params.onSuccess,
        error: function (e) {
            // console.log(e);
            if(e.status !== 200){
                if(typeof params.onError === "function") params.onError(e.responseJSON);
            }
        }
    });
}

// Ajax Request Multipart (FormData/Files)
Frameduz.prototype.sendMultipart = function(type, params){
    $.ajax({
        type: type,
        url: params.url,
        enctype: "multipart/form-data",
        data: new FormData(params.data),
        headers: params.header,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        datatype: "json",
        xhr: function () {
            var jxhr = null;
            if(window.ActiveXObject) 
                jxhr = new window.ActiveXObject( "Microsoft.XMLHTTP" );
            else
                jxhr = new window.XMLHttpRequest();

            if(jxhr.upload) {
                jxhr.upload.addEventListener("progress", function (evt) {
                    if(evt.lengthComputable) {
                        let percent = Math.round((evt.loaded / evt.total) * 100);
                        if(typeof params.onProgress === "function") params.onProgress(percent);
                    }
                }, false);
            }
            return jxhr;
        },
        success: params.onSuccess,
        error: function (e) {
            // console.log(e);
            if(e.status !== 200){
                if(typeof params.onError === "function") params.onError(e.responseJSON);
            }
        }
    });
}

Frameduz.prototype.loadTable = function(params) {
    const app = this;
    table.loader().show();
    table.empty().hide();
    table.content.hide();
    table.paging.hide();
    setTimeout(function(){
        app.sendRequest("POST", {
            url: params.url, 
            header: params.header, 
            data: params.data,
            onSuccess: function(response) {
                // console.log(response);
                app.createTable(response.data, params);
            },
            onError: function(error) {
                // console.log(error);
                alert(error.message);
            }
        });
    }, 500);
}

Frameduz.prototype.createTable = function(data, params) {
    table.loader().hide();
    table.empty().hide();

    let query = data.query;
    let contents = data.contents;
    let number = data.number;
    let page = parseInt(data.page);
    let size = data.size;
    let total = data.total;
    let totalpages = data.totalpages;

    if (query != "") {
        table.query.html("<code>"+query+"</code>");
    }

    table.totalData.html(total);
    if (total > 0) {
        // Create Table Contents
        table.contentTbody.html("");
        for (let rows in contents) {
            let row = table.contentTbodyRows.clone();
            let result = row.html().replace("{number}", number++);
            json = contents[rows];
            for (key in json) {
                var find = new RegExp("{"+key+"}", "g");
                result = result.replace(find, json[key]);
                row.html(result);
            }
            table.contentTbody.append(row);
        }
        table.content.show();
        if(typeof params.onShow === "function") {
            table.content.find("img").each(function(idx, img) {
                const _src = this.getAttribute("_src");
                this.setAttribute("src", _src);
            });
            params.onShow(table.content);
        }

        // Create Paging
        table.paging.html("");
        if (totalpages > 1) {
            const prev_page = (page > 1) ? page - 1 : 1;
            const next_page = (page < totalpages) ? page + 1 : totalpages;
            
            const btn_page = table.pagingItem.html();
            const btn_first = table.pagingItem.clone().html(btn_page.replace("{page}", "&laquo;")).find("a").attr("page-number", 1).parent();
            const btn_last = table.pagingItem.clone().html(btn_page.replace("{page}", "&raquo;")).find("a").attr("page-number", totalpages).parent();
            const btn_prev = table.pagingItem.clone().html(btn_page.replace("{page}", "&lsaquo;")).find("a").attr("page-number", prev_page).parent();
            const btn_next = table.pagingItem.clone().html(btn_page.replace("{page}", "&rsaquo;")).find("a").attr("page-number", next_page).parent();
            const btn_dots = table.pagingItem.clone().html(btn_page.replace("{page}", "...")).addClass("disabled");
            const btn_active = table.pagingItem.clone().addClass("active").html(btn_page.replace("{page}", page)).find("a").attr("page-number", "").parent();
            
            if (page > 3) {
                table.paging.append(btn_first);
                table.paging.append(btn_prev);
                table.paging.append(btn_dots);
            }

            for (let p = (page - 2); p < page; p++) {
                if (p < 1) continue;
                let pages = table.pagingItem.clone().html(btn_page.replace("{page}", p)).find("a").attr("page-number", p).parent();
                table.paging.append(pages);
            }

            table.paging.append(btn_active);

            for (let p = (page + 1); p < (page + 3); p++) {
                if (p > totalpages) break;
                let pages = table.pagingItem.clone().html(btn_page.replace("{page}", p)).find("a").attr("page-number", p).parent();
                table.paging.append(pages);
            }

            if ((page + 2) < totalpages) {
                table.paging.append(btn_dots);
            }

            if (page < (totalpages - 2)) {
                table.paging.append(btn_next);
                table.paging.append(btn_last);
            }

            table.paging.show();
            table.paging.find("a[page-number!='']").on("click", function(event) {
                event.preventDefault();
                const page_number = $(this).attr("page-number")
                if(typeof params.onPage === "function") params.onPage(page_number);
            })

        }
    }
    else {
        table.empty().show();
    }
}

Frameduz.prototype.loadForm = function(params) {
    const app = this;
    const form_content = app.form.clone();
    form_content.attr("id", "form-input");
    setTimeout(function(){
        app.sendRequest("GET", {
            url: params.url, 
            header: params.header, 
            data: params.data,
            onSuccess: function(response) {
                // console.log(response);
                const data = response.data;
                if(typeof params.onShow === "function") params.onShow(form_content, data);
                form_content.on("submit", function(event) {
                    event.preventDefault();
                    if(typeof params.onSubmit === "function") params.onSubmit(form_content);
                });
            },
            onError: function(error) {
                // console.log(error);
                alert(error.message);
            }
        });
    }, 500);
}

Frameduz.prototype.createForm = function(form_content, object) {
    $.each(object, (key, val) => form_content.find("span[data-form-object='"+key+"']").replaceWith(val));
}

Frameduz.prototype.createProgress = function(obj) {
    const progress = $("<div>").addClass("fzprogress");
    const spinner = $("<div>").addClass("spinner");
    for (let i = 1; i <= 12; i++) {
        $("<div>").addClass("bar"+i).appendTo(spinner);
    }
    spinner.appendTo(progress);
    obj.css("position", "relative").prepend(progress);
    return progress;
}

Frameduz.prototype.createDialog = function(id) {
    const modal = $(id);
    modal.title = modal.find(".modal-title");
    modal.content = modal.find(".modal-content");
    modal.body = modal.find(".modal-body");
    modal.footer = modal.find(".modal-footer");
    modal.submit = modal.footer.find("button[type='submit']");
    modal.setSize = function(size) {
        modal.find(".modal-dialog").removeClass("modal-sm, modal-lg");
        if (size == "small") {
            modal.find(".modal-dialog").addClass("modal-sm");
        }
        else if (size == "large") {
            modal.find(".modal-dialog").addClass("modal-lg");
        }
    }

    return modal;
}

// Create Form Element
Frameduz.prototype.formel = {
    key: function(id, value){
        return $("<input>").attr({type: "hidden", id: id, name: id, value: value});
    },
    input: function(id, type, value){
        return $("<input>").attr({type: type, id: id, name: id, value: value});
    },
    textArea: function(id, value){
        return $("<textarea>").attr({id: id, name: id, rows: 5}).html(value);
    },
    select: function(id, data, value){
        var group = $("<div>");
        var select = $("<select>").attr({id: id, name: id});
        var value = (value instanceof Array) ? value : [value];
        $.each(data, function(key, val){
            var option = $("<option>").attr({value: key, selected: ($.inArray(key, value) != -1)}).text(val.text)
            select.append(option);
        });
        group.append(select);
        return group.children();
    },
    radio: function(id, data, value){
        var group = $("<div>");
        $.each(data, function(key, val){
            var radio = $("<input>").attr({type: "radio", id: id, name: id, value: key, checked: (key == value)});
            var label = $("<label>").attr("style", "color: #000; cursor: pointer; margin: 10px;").append(radio).append(" "+val.text);
            group.append(label);
        });
        return group.children();
    },
    checkbox: function(id, data, value){
        var group = $("<div>");
        var value = (value instanceof Array) ? value : [value];
        $.each(data, function(key, val){
            var check = $("<input>").attr({type: "checkbox", id: id, name: id, value: val, checked: ($.inArray(val, value) != -1)});
            var label = $("<label>").attr("style", "color: #000; cursor: pointer; margin: 5px;").append(check).append(" "+val);
            group.append(label);
        });
        return group.children();
    },
    uploadImage: function(id, image, mimes, desc){
        var group = $("<div>");
        var preview = $("<div>").attr({class: "image-preview"}).html('<img src="'+image+'" class="img-responsive thumbnail" alt="image" width="100%">');
        var file = $("<input>").attr({type: "file", id: id, name: id, class: "file-image", style: "display: none;", accept: mimes});
        var button = $("<label>").attr({for: id, class: "btn btn-block btn-sm btn-dark", style: "cursor: pointer; margin-top: 10px;"}).html("UPLOAD");
        var desc = $("<small>").attr({class: "help-block"}).html(desc);
        group.append(preview).append(file).append(button).append(desc);
        return group.html();
    },
    imagePreview: function(obj){
        if(obj.files && obj.files[0]){
            var reader = new FileReader();
            var preview = $(obj).siblings(".image-preview");
            reader.onload = function(e){
                preview.html('<img src="'+e.target.result+'" class="img-responsive thumbnail" alt="image" width="100%">');
            };
            reader.readAsDataURL(obj.files[0]);
        }
    },
}

/**
 * JQuery Prototype convert to json object
 */

 $.fn.serializeObject = function(){

    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate" : /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push": /^$/,
            "fixed": /^\d+$/,
            "named": /^[a-zA-Z0-9_]+$/
        };

    this.build = function(base, key, value){
        base[key] = value;
        return base;
    };

    this.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function(){
        // Skip invalid keys
        if(!patterns.validate.test(this.name)){ return; }
        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while((k = keys.pop()) !== undefined){
            // Adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
            // Push
            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }
            // Fixed
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }
            // Named
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }
        json = $.extend(true, json, merge);
    });

    return json;
};