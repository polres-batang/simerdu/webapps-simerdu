<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Auth.php
 */

namespace core;
use core\exception\{AuthException};

class OAuth extends Singleton {
    
    /**
     * JWT Format
     * header.payload.signature
     * signature = base64UrlEncode(hashedData)
     * hashedData = hash(data, secret)
     * data = base64UrlEncode(header).base64UrlEncode(payload)
     */
    
    protected $cookie = '';
    protected $secret_key = '';
    protected $refresh_key = '';
    protected $signature = '';

    public function __construct(string $oauth_service) {
        $this->Url = new Url();
        $this->Config = $this->Url->getConfig();
        if (!isset($this->Config['oauth'][$oauth_service])) {
            throw new \Exception('Config Auth: '.$oauth_service.' not found');
            
        }
        $this->ConfigAuth = $this->Config['oauth'][$oauth_service];
        $this->cookie = $this->ConfigAuth['cookie'];
        $this->secret_key = $this->ConfigAuth['jwt_access_token_secret'];
        $this->refresh_key = $this->ConfigAuth['jwt_refresh_token_secret'];
    }

    private function createJwtSignature(array $header, array $payload, string $key) {
        // Encode Header
        $base64UrlHeader = Helper::base64UrlEncode(json_encode($header));
        // Encode Payload
        $base64UrlPayload = Helper::base64UrlEncode(json_encode($payload));
        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader.'.'.$base64UrlPayload, $key, true);
        // Encode Signature to Base64Url String
        $base64UrlSignature = Helper::base64UrlEncode($signature);
        return [$base64UrlHeader, $base64UrlPayload, $base64UrlSignature];
    }

    /**
     * Setting Expiry:
     * years|month|week|days|hours|minutes|seconds
     * +1 week 3 days 7 hours 5 seconds
     * +5 minutes
     */
    protected function jwtToken(array $payload, string $key, string $expiry = '+1 hours') {
        $header = [
            'typ' => 'JWT',
            'alg' => 'HS256' // using HMAC 256
        ];
        $payload['expiry'] = strtotime($expiry, strtotime('now'));
        
        // Create JWT
        list($base64UrlHeader, $base64UrlPayload, $base64UrlSignature) = $this->createJwtSignature($header, $payload, $key);
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
        return $jwt;
    }

    protected function jwtValidate(string $jwt, string $key) {
        if (empty($jwt)) {
            throw new AuthException('Token Not Found ...');
        }
        
        $jwt = explode('.', $jwt);
        if (count($jwt) != 3) {
            throw new AuthException('Token Invalid ...');
        }
        
        list($header, $payload, $signatureProvided) = $jwt;
        $header = json_decode(base64_decode($header), true);
        $payload = json_decode(base64_decode($payload), true);
        
        // check expirate token
        $expiry = intval($payload['expiry']);
        if ((strtotime('now') > $expiry)) {
            throw new AuthException('Token Expirated ...');
        }

        // build a signature based on the header and payload using the secret
        list($base64UrlHeader, $base64UrlPayload, $base64UrlSignature) = $this->createJwtSignature($header, $payload, $key);

        // verify it matches the signature provided in the token
        if ($base64UrlSignature !== $signatureProvided) {
            throw new AuthException('Token Invalid ...');
        }

        return $payload;
    }

    /**
     * setCookie(name, value, expire, path, domain, secure, httponly)
     * - path: tempat dimana cookie bisa digunakan default / (seluruh bagian web)
     * - domain: dapat diisi subdomain yang dapat menggungkan cookie tersebut.
     * - secure: default false, jika true browser akan mengirim cookie jika koneksi https
     * - httponly: default false, jika true, cookie hanya bisa diakses dari php, javascript tidak bisa baca
     */
    protected function setCookieToken(string $value) {
        setcookie($this->cookie, $value, time() + COOKIE_EXP, '/', '', false, true); // JS tidak bisa baca cookie
        // setcookie($this->cookie, $value, time() + COOKIE_EXP, '/', '', false, false);
    }

    protected function removeCookieToken() {
        unset($_COOKIE[$this->cookie]);
        setcookie($this->cookie, '', time() - COOKIE_EXP, '/');
    }

    // JSON RAW
    protected function getBody(array $model = []) {
        return Helper::getBody($model);
    }

    // HEADERS
    protected function getHeaders(array $model = []) {
        return Helper::getHeaders($model);
    }

    // GET
    protected function getParams(array $model = []) {
        return Helper::getParams($model);
    }

    // POST
    protected function getFields(array $model = []) {
        return Helper::getFields($model);
    }
    
    protected function getCookieToken() {
        $token = '';
        if (isset($_COOKIE[$this->cookie])) {
            $token = $_COOKIE[$this->cookie];
        }

        return $token;
    }

    protected function getBearerToken() {
        $token = '';
        // $header = getallheaders();
        // $header = array_change_key_case($header, CASE_LOWER);
        $header = $this->getHeaders();
        if (!isset($header['authorization'])) {
            throw new AuthException('Access Denied !!');
        }
        
        if (preg_match('/Bearer\s(\S+)/', $header['authorization'], $matches)) {
            $token = $matches[1];
        }

        if (empty($token)) {
            throw new AuthException('Access Denied !!');
        }

        return $token;
    }

    // Overide untuk proses login
    public function setValidate() {
        
    }

    // Digunakan untuk proses check session pada aplikasi web
    public function getSession() {
        $token = '';
        $payload = [];
        $isValid = false;
        try {
            $token = $this->getCookieToken();
            $payload = $this->jwtValidate($token, $this->secret_key);
            $isValid = true;
        } catch (\Throwable $th) {
            $isValid = false;
        }
        
        // return [$token, $payload, $isValid];
        return json_decode(json_encode([
            'token' => $token,
            'data' => $payload,
            'isValid' => $isValid
        ]));
    }

    // Digunakan untuk proses logout
    public function checkout() {
        $this->removeCookieToken();
    }

    // Digunakan untuk proses check session pada api
    public function getAuthorization() {
        $token = $this->getBearerToken();
        $payload = $this->jwtValidate($token, $this->secret_key);
        return [$token, $payload];
    }

    protected function showResponse(array $responseMessage) {
        Helper::showResponse($responseMessage);
    }

    public function showError(string $message = 'Unauthorized', $code = 401) {
        Helper::errorResponse($message, $code);
    }

}
?>