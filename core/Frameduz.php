<?php 
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Frameduz.php
 */

namespace core;
use core\exception\{
    NotFoundException, 
    ConnectionException, 
    ValidationException,
    RequestException,
};

class Frameduz extends Singleton {

    private $routes = [];
    private $not_found = true;
    protected $Config = [];
    
    public function __construct() {
        $this->Url = new Url();
        $this->Config = $this->Url->getConfig();
    }

    protected function requestMapping(string $method, string $endpoint, callable $callback, string $name = '') {
        $endpoint = implode('/', ['/', $this->getProject(), $this->getController(), $endpoint] );
        $endpoint = preg_replace('#/+#','/',$endpoint); // remove duplicate /
        $endpoint = rtrim($endpoint, '/');
        $method = strtoupper($method);
        $this->routes[] = ['name' => $name, 'method' => $method, 'endpoint' => $endpoint, 'callback' => $callback];
    }

    // Static function agar, dipanggil cukup sekali
    public function execute() {
        $reqMet = $_SERVER['REQUEST_METHOD'];
        $reqUrl = $_SERVER['REQUEST_URI']; // v1 (Design (/) sebagai default project & controller tida terdeteksi)
        $reqUrl = $this->getReqUrl(); // v2 (perbaikan v1)
        $reqUrl = current(explode('?', $reqUrl)); // Remove Query Params ex. ?page=xxx

        $pathVariable = [
            '/\\\:[a-zA-Z0-9\_\-]+/', // :id
            '/\\\{[a-z]+\\\}/', // {id}
        ];
        
        $this->addOauthRoute();
        $this->addDefaultRoute();
        foreach ($this->routes as $key => $map) {
            // convert urls like '/users/{uid}/posts/{pid}' to regular expression
            $pattern = '@^'.preg_replace($pathVariable, '([a-zA-Z0-9\-\_\.\@]+)', preg_quote($map['endpoint'])).'$@D';
            if ($reqMet == $map['method'] && preg_match($pattern, $reqUrl, $matches)) {
                $this->not_found = false;
                // remove the first match
                $this->currentPath = $matches[0];
                array_shift($matches); // [0] => current path
                return call_user_func_array($map['callback'], $matches);
            }
        }

        if ($this->not_found) {
            throw new NotFoundException('Request: '.$reqUrl.' Not Found');
            // $this->getRoute();
        }
    }

    public function addOauthRoute() {
        $oauth = $this->getOauthList();
        foreach ($oauth as $key => $value) {
            $this->routes[] = ['name' => $key, 'method' => 'GET', 'endpoint' => '/oauth'.$value['endpoint'], 'callback' => ''];
        }
    }

    public function addDefaultRoute() {
        $this->requestMapping('GET', '/route', function() {
            $this->getRoute();
        }, 'API SPECS');

        $this->requestMapping('GET', '/create/randomid', function() {
            echo 'Random ID: '.Helper::createRandomID();
        }, 'Tools: Random ID');

        $this->requestMapping('GET', '/create/randomkey', function() {
            echo 'Random Key: '.Helper::createRandomKey();
        }, 'Tools: Random Key');
        
        $this->requestMapping('GET', '/create/uuid', function() {
            echo 'UUID v4: '.Helper::UUIDv4();
        }, 'Tools: UUID v4');

        $this->requestMapping('GET', '/users/:uid/posts/:pid', function($uid, $pid) {
            Helper::$responseMessage['data'] = [
                'method' => 'GET',
                'uid' => $uid,
                'pid' => $pid,
                'body' => $this->getBody(),
                'headers' => $this->getHeaders(),
                'fields' => $this->getFields(),
                'params' => $this->getParams(),
                'settings' => $this->getSettingList(),
                'projects' => $this->getProjectList(),
                'templates' => $this->getTemplateList(),
                'baseUrl' => $this->getBaseUrl(),
                'activeUrl' => $this->getActiveUrl(),
                'currentPath' => $this->getCurrentPath(),
                'getProjectName' => $this->getProject(),
                'getControllerName' => $this->getControllerName(),
                'getController' => $this->getController(),
                'createLink' => $this->createLink(),
                'createLinkCustom' => $this->createLink([$this->getProject(), $this->getController(), 'custom']),
            ];
            $this->showResponse(Helper::$responseMessage);
        }, 'Test Route');
    }

    protected function getRoute() {
        $result = [];
        foreach ($this->routes as $key => $map) {
            unset($map['callback']);
            $map['path_url'] = $this->getBaseUrl().$map['endpoint'];
            array_push($result, $map);
        }

        Helper::$responseMessage['message'] = 'Api Specs';
        Helper::$responseMessage['data']['baseurl'] = $this->getBaseUrl();
        Helper::$responseMessage['data']['route'] = $result;
        Helper::showResponse(Helper::$responseMessage);
    }

    protected function filterParams(array $params, array $input) {
		return Helper::filterParams($params, $input);
	}

    // JSON RAW
    protected function getBody(array $model = []) {
        return Helper::getBody($model);
    }

    // HEADERS
    protected function getHeaders(array $model = []) {
        return Helper::getHeaders($model);
    }

    // GET
    protected function getParams(array $model = []) {
        return Helper::getParams($model);
    }

    // POST
    protected function getFields(array $model = []) {
        return Helper::getFields($model);
    }

    // FILES
    protected function getFiles(array $model = []) {
        return Helper::getFiles($model);
    }

    public function getBaseUrl() {
        return $this->Url->getBaseUrl();
    }

    public function getActiveUrl() {
        return $this->Url->getActiveUrl();
    }

    public function getReqUrl() {
        return $this->Url->getReqUrl();
	}

    public function getProjectName() {
        return $this->Url->getProjectName();
    }

    public function getControllerName() {
        return $this->Url->getControllerName();
    }

    public function getPathController() {
        return $this->Url->getPathController();
    }

    public function getProject() {
        return $this->Url->getProject();
    }

    public function getController() {
        return $this->Url->getController();
    }

    public function getCurrentPath() {
        return $this->currentPath;
    }

    public function getSettingList() {
        return $this->Config['settings'];
    }

    public function getProjectList() {
        return $this->Config['projects'];
    }

    public function getTemplateList() {
        return $this->Config['templates'];
    }

    public function getOauthList() {
        return $this->Config['oauth'];
    }

    public function createLink(array $location = []) {
        if (count($location) > 0) {
            array_unshift($location, '');
        }

        $location = implode('/', $location);
        $location = preg_replace('#/+#','/',$location); // remove duplicate /
        $location = rtrim($location, '/');
        return $this->getBaseUrl().$location;
    }

    public function redirect(array $location = []) {
        $redirect_link = $this->createLink($location);
        header('Location: '.$redirect_link, true, 302);
    }

    /**
     * Upload Files
     */
    public function upload(string $name, array $options = []) {
        $result = [
            'data' => [],
            'file_upload' => '',
            'path_upload' => '',
        ];

        $options = $this->filterParams([
            'path_upload' => 'upload/image/',
            'file_type' => 'jpeg|jpg|png',
            'max_size' => 5*(1024*1024), // default max size: 5mb
        ], $options);
        
        list($path_upload, $file_type, $max_size) = array_values($options);
        
        $files = $this->getFiles();
        if (isset($files[$name]) && !empty($files[$name]['name']) && $files[$name]['size'] > 0) {
            $file = $files[$name];
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            
            $result['data'] = $file;
            $result['file_upload'] = Helper::createRandomID(15).'.'.$ext;
            $result['path_upload'] = $path_upload;

            $path_upload = ltrim($path_upload, '/'); // hapus / didepan karena lokasi folder berada di root, bukan relatif dengan template
            if (!is_dir($path_upload)) {
                throw new NotFoundException('Folder tujuan tidak ditemukan');
                // throw new NotFoundException(json_encode($options));
            }

            if (!preg_match( '!\.(' . $file_type . ')$!i', $file['name'])) {
                throw new ValidationException('Hanya format file: '.$file_type.' yang diijinkan');
            }

            if ($file['size'] > $max_size) {
                $mb = ($max_size/1024/1024).'mb';
                throw new ValidationException('Ukuran file terlalu besar, max: '.$mb);
            }

            $upload = false;
            $upload = move_uploaded_file($file['tmp_name'], $path_upload.$result['file_upload']);
            if (!$upload) {
                throw new RequestException('File gagal diupload');
            }
        }

        return $result;

    }
    
}
?>