<?php 

return [
    'application' => [
        'project' => 'publish', // Set Default Project (Base Url)
    ],
    'settings' => [ // List Of Setting
        'web' => [
            'title' => 'SIMERDU',
            'author' => 'Polres Batang',
            'description' => 'Sistem Informasi & Management Emergency Terpadu',
            'keywords' => 'simerdu, polres, batang, satlantas',
        ],
        'upload' => [
            'path_upload' => '/upload/image/',
            'path_upload_laporan' => '/upload/image/laporan/',
            'path_upload_broadcast' => '/upload/image/broadcast/',
            'file_type' => 'jpeg|jpg|png',
            'max_size' => 5*(1024*1024),
        ],
        'socket' => base64_encode(json_encode(
            [
                'server' => 'https://adolbots2.herokuapp.com',
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.js',
                'token' => 't0khbh6g9ukek2b0hmnbtreqh0', // Token Akses ke Web Socket
                'room' => 'simerdu', // Default Room
            ]
        )),
    ],
    'projects' => [ // List Of Projects
        'api' => [
            'path' => 'backend',
            'controller' => 'v1', // Set Default Controller
        ],
        'publish' => [
            'path' => 'frontend',
            'controller' => 'main', // Set Default Controller
        ],

    ],
    'templates' => [ // List Of Templates
        'bootstrap' => [
            'basepath' => '',
            'favicon' => '',
            'css' => [
                'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css',
            ],
            'js' => [
                'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js',
            ],
        ],
        'appui' => [
            'basepath' => 'appui-27/',
            // 'favicon' => '/#',
            'favicon' => '/asset/image/favicon_io/favicon.ico',
            'meta' => [],
            'css' => [
                'css/bootstrap.min.css',
                'css/plugins.css',
                'css/main.css',
                'css/themes/amethyst.css',
                'css/themes.css',
                'plugins/daterangepicker/daterangepicker.css',
                // 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css',
            ],
            'js' => [
                'js/vendor/modernizr-3.3.1.min.js',
                'js/vendor/jquery-2.2.4.min.js',
                'js/vendor/bootstrap.min.js',
                'js/plugins.js',
                'js/app.js',
                'plugins/daterangepicker/moment.min.js',
                'plugins/daterangepicker/daterangepicker.min.js',
                'plugins/highcharts/highcharts.js',
                'plugins/highcharts/exporting.js',
                'plugins/highcharts/export-data.js',
                // 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js',
                // 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js',
                // 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js',
            ],
        ],
    ],
    'fcm' => [
        'topic' => 'simerdu-polres-batang',
        'apikey' => 'AAAA3W5ZY1Q:APA91bGUwnpfBdeZ9hio10w9XWdxQ4H9mAc7T1ZbejVQH1E8zALi2BNC8NfVkq-qvVH1uPYSHekjpyG0m9PCtwavj1QKxEABLjOMKoSlw5PfuAb4je1vtZ7JaPrRVnp0bfEjJT8_LYl3',
    ],
    'oauth' => [
        'user_auth' => [
            'endpoint' => '/user/auth',
            'service' => 'UserAuth',
            'cookie' => 'UserAuth',
            'jwt_access_token_secret' => $_ENV['JWT_ACCESS_TOKEN'] ?? '01b4876a-20bc-455c-beed-b3a0e147b81c', // UUID v4
            'jwt_refresh_token_secret' => $_ENV['JWT_REFRESH_TOKEN'] ?? 'ebe2ed99a6f6dd4a529ae2ef77032ef704756c72cfd2759003e864a72942ebba', // Random Key
        ]
    ]
];

?>