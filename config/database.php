<?php 

return [
    'callcenter' => [
        'path' => $_ENV['JSON_PATH'] ?? 'callcenter.json',
    ],
    'region' => [
        'path' => $_ENV['JSON_PATH'] ?? 'region.json',
    ],
    'user' => [
        'path' => $_ENV['JSON_PATH'] ?? 'user.json',
    ],
    'mysqldb' => [
        'driver' => 'mysql',
        'host' => $_ENV['MYSQL_HOST'] ?? 'localhost',
        'port' => $_ENV['MYSQL_PORT'] ?? '3306',
        'user' => $_ENV['MYSQL_USER'] ?? 'simerdup',
        'password' => $_ENV['MYSQL_PASS'] ?? 'GO88=B=CAK9(',
        'dbname' => $_ENV['MYSQL_DBNAME'] ?? 'simerdup_database',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'errorMsg' => 'Maaf, Gagal terhubung dengan database',
    ],
];

?>