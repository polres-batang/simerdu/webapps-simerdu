<?php 
namespace oauth;
use core\Helper;
use app\backend\repository\{User, Register};

class UserAuth extends \core\OAuth {

    public function __construct() {
        parent::__construct('user_auth');
    }

    // Overide untuk proses login
    public function setValidate() {
        $user = new User();
        $fields = $this->getFields(['username' => '', 'password' => '']);
        list($username, $password) = array_values($fields);
        $params = [
            'username' => $username,
            'password' => $user->encrypt($password),
        ];

        $result = $user->getQuery('AND', $params);
        if ($result['count'] > 0) {
            $data = current($result['value']);
            unset($data['password']);

            // create jwt token
            $access_token = $this->jwtToken($data, $this->secret_key, '+7 days');
            // save token to cookie
            $this->setCookieToken($access_token);

            Helper::$responseMessage['code'] = 200;
            Helper::$responseMessage['status'] = 'success';
            Helper::$responseMessage['message'] = 'Login success ..';
            Helper::$responseMessage['data'] = $data;
            $this->showResponse(Helper::$responseMessage);
        }
        else {
            throw new \core\exception\ValidationException('Login failed !!');
        }

        /**
         * Simple Version
         */
        // if ($username == 'admin' && $password == 'admin') {
        //     $jwt_access_token = $this->jwtToken(
        //         ['user' => 'Administrator', 'email' => 'admin@simerdu.id'],
        //         $this->secret_key,
        //         '+1 days'
        //     );
            
        //     $this->setCookieToken($jwt_access_token);
        //     Helper::$responseMessage['code'] = 200;
        //     Helper::$responseMessage['status'] = 'success';
        //     Helper::$responseMessage['message'] = 'Login success ..';
        //     Helper::$responseMessage['data'] = $fields;
        //     $this->showResponse(Helper::$responseMessage);
        // }
        // else {
        //     throw new \core\exception\ValidationException('Login failed !!');
        // }
    }

    public function mobileValidate() {
        $register = new Register();
        $fields = $this->getFields(['username' => '', 'password' => '']);
        list($username, $password) = array_values($fields);
        $params = [
            'nomer_handphone' => $username,
            'password' => $register->encrypt($password),
        ];

        list($conditions, $params) = $register->createQueryParams($params, '=');
        $where = ($params) ? ' WHERE '.implode(' AND ', $conditions) : '';
        $query = 'SELECT * FROM '.$register->getTable().$where;
        $result = $register->getQuery($query, $params);
        if ($result['count'] > 0) {
            $data = current($result['value']);
            $data['access_token'] = $this->jwtToken($data, $this->secret_key, '+30 days');
            unset($data['password']);
            Helper::$responseMessage['code'] = 200;
            Helper::$responseMessage['status'] = 'success';
            Helper::$responseMessage['message'] = 'Login success ..';
            Helper::$responseMessage['data'] = $data;
            $this->showResponse(Helper::$responseMessage);
        }
        else {
            throw new \core\exception\ValidationException('Login failed !!');
        }
    }

}

?>